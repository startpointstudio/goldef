<section class="partners">
    <div class="container">
        <?php if($documentObject['id'] != '5'): ?>
        <div class="blue-title"><?php echo e($modx->getConfig("__Our_partners")); ?></div>
        <?php else: ?>
            <h1 class="blue-title"><?php echo e($modx->getConfig("__Our_partners")); ?></h1>
        <?php endif; ?>
        <div class="owl-carousel owl-theme">
            <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="partnersItem">
                    <div class="img">
						<a href="<?php echo e($partner['tv_partnerslink']); ?>" target="_blanc"><img src="<?php echo e($partner['tv_img']); ?>" alt=""></a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section><?php /**PATH /var/www/startpont/data/www/goldef.startpointstudio.com/views/partials/partnersBlock.blade.php ENDPATH**/ ?>