<section class="rewievs-wrap">
    <div class="container">
        <div class="blue-title"><?php echo e($modx->getConfig("__Reviews")); ?></div>
        <div class="rewievsItems">
            <div class="owl-carousel owl-theme">
            <?php echo e($a=1); ?>

            <?php $__currentLoopData = $reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($a==1): ?>
                    <div class="rewievsItem first">
                <?php elseif($a>2): ?>
                    <div class="rewievsItem last">
                <?php else: ?>
                    <div class="rewievsItem">
                <?php endif; ?>
                        <img data-aos="zoom-in" src="<?php echo e($review['tv_img']); ?>">
                        <div data-aos="fade-up" > <?php echo $review['tv_content_'.$lang]; ?> </div>
                    </div>
                    <?php echo e($a=$a+1); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</section><?php /**PATH /var/www/startpont/data/www/goldef.startpointstudio.com/views/partials/rewievsBlock.blade.php ENDPATH**/ ?>