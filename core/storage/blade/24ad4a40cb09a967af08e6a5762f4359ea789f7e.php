<?php $__env->startSection('content'); ?>
    <section class="slider-wrap home">
        <div class="hide"><?php echo e($a=1); ?></div>
        <div class="owl-carousel">

            <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($a==1): ?>
                    <div class="owlItem <?php echo e($slide['id']); ?>" style="background-image: url('<?php echo e($slide['img']); ?>'); background-image: url('<?php echo e($slide['img_webp']); ?>')">
                        <div class="container">
                            <div class="section-tovar">
                                <img  class="" src="<?php echo $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']); ?>" alt="" >
                                
                            </div>
                            <div class="text-left active">
                                <p><?php echo e($slide['text']); ?></p>
                            </div>
                        </div>
                        <div class="hide"><?php echo e($a=$a+1); ?></div>
                    </div>
                <?php else: ?>
                    <div class="owlItem <?php echo e($slide['id']); ?>" style="background-image: url('<?php echo e($slide['img']); ?>'); background-image: url('<?php echo e($slide['img_webp']); ?>')">
                        <div class="container">
                            <div class="section-tovar">
                                <img  class="hide" src="<?php echo $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']); ?>" alt="" >
                                
                            </div>
                            <div class="text-left">
                                <p><?php echo e($slide['text']); ?></p>
                            </div>
                        </div>
                        <div class="hide"><?php echo e($a=$a+1); ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </section>

    <section class="icons-wrap text-center">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-recycle"></i>
                        </div>
                        <div class="ico-title">
                            <?php echo e($modx->getConfig("__Full_production_cycle")); ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fab fa-battle-net"></i>
                        </div>
                        <div class="ico-title">
                            <?php echo e($modx->getConfig("__Innovative_technologies")); ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-globe-americas"></i>
                        </div>
                        <div class="ico-title">
                            <?php echo e($modx->getConfig("__World_recognition")); ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-chart-line"></i>
                        </div>
                        <div class="ico-title">
                            <?php echo e($modx->getConfig("__Efficiency")); ?>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="goodsItem-wrap">
        <div class="container">
            <div class="row">

                <?php $__currentLoopData = $productsOnMain; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="product_item col-md-3">
                        <div class="product_img">
                            <a href="<?php echo e($root.$modx->makeUrl($product['id'])); ?>"><img src="<?php if($lang == 'en'): ?><?php echo $product['tv_img_en']; ?><?php else: ?><?php echo $product['tv_img']; ?><?php endif; ?>" alt="<?php echo e($product['tv_pagetitleonmain_'.$lang]); ?>"></a>
                        </div>
                        <div class="product_title">
                            <a href="<?php echo e($root.$modx->makeUrl($product['id'])); ?>"><?php echo e($product['tv_pagetitleonmain_'.$lang]); ?></a>
                        </div>
                    </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
        </div>
    </section>

    <section class="goodsItem-wrap">
        <div class="tabs">
            <div class="container">
                <div class="tab tab3">
                    <input type="radio" id="tab3" name="tab-group" checked>
                    <label for="tab3" class="tab-title three"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                <?php $__currentLoopData = $catalogAntiseptik; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="tadItem hide">
                                        <a href="<?php echo e($root.$goodsItem['url']); ?>">
                                            <img src="<?php if($lang == 'en'): ?><?php echo $goodsItem['tv_img_en']; ?><?php else: ?><?php echo $goodsItem['tv_img']; ?><?php endif; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab tab1">
                    <input type="radio" id="tab1" name="tab-group">
                    <label for="tab1" class="tab-title one"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                <?php $__currentLoopData = $dogCatalogIndexg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsItemg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="tadItem hide">
                                        <a href="<?php echo e($root.$goodsItemg['url']); ?>">
                                            <img src="<?php if($lang == 'en'): ?><?php echo $goodsItemg['tv_img_en']; ?><?php else: ?><?php echo $goodsItemg['tv_img']; ?><?php endif; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $dogCatalogIndexs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsItems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="tadItem hide">
                                        <a href="<?php echo e($root.$goodsItems['url']); ?>">
                                            <img src="<?php if($lang == 'en'): ?><?php echo $goodsItems['tv_img_en']; ?><?php else: ?><?php echo $goodsItems['tv_img']; ?><?php endif; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $dogCatalogIndexu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsItemu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="tadItem hide">
                                        <a href="<?php echo e($root.$goodsItemu['url']); ?>">
                                            <img src="<?php if($lang == 'en'): ?><?php echo $goodsItemu['tv_img_en']; ?><?php else: ?><?php echo $goodsItemu['tv_img']; ?><?php endif; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab tab2">
                    <input type="radio" id="tab2" name="tab-group">
                    <label for="tab2" class="tab-title two"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                <?php $__currentLoopData = $catCatalogIndex; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="tadItem hide">
                                        <a href="<?php echo e($root.$goodsItem['url']); ?>">
                                            <img src="<?php if($lang == 'en'): ?><?php echo $goodsItem['tv_img_en']; ?><?php else: ?><?php echo $goodsItem['tv_img']; ?><?php endif; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </section>
    <?php echo $__env->make("partials.rewievsBlock", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make("partials.videoRewievs", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make("partials.partnersBlock", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/startpont/data/www/goldef.startpointstudio.com/views/home2.blade.php ENDPATH**/ ?>