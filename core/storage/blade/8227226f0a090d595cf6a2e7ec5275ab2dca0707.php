<!DOCTYPE html>
<html lang="uk">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162514710-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162514710-1');
    </script>

    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php $__env->startSection('keywords'); ?><?php echo evo_parser($documentObject['keyw_'.$lang]);?><?php echo $__env->yieldSection(); ?>">
    <meta name="description" content="<?php $__env->startSection('description'); ?><?php echo evo_parser($documentObject['desc_'.$lang]);?><?php echo $__env->yieldSection(); ?>">
    <base href="<?php echo e($modx->getConfig("site_url")); ?>"/>

    <link href="theme/css/all.css" rel="stylesheet">

    <?php if($documentObject['template'] != 4): ?>
        <link rel="stylesheet" href="theme/css/style-main.css?v=4">
    <?php endif; ?>
    <title>
        <?php $__env->startSection('title'); ?>
            <?php echo evo_parser($documentObject['titl_'.$lang]);?>
        <?php echo $__env->yieldSection(); ?>
    </title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<body
<?php if($documentObject['template'] == 9): ?>
    class="contact-wrap"
<?php elseif($documentObject['template'] == 10 ): ?>
    class="partners-wrap"
<?php elseif($documentObject['template'] == 11 ): ?>
    class="partners-wrap about"
<?php elseif($documentObject['template'] != 1): ?>
    class="bg-wrapp"
<?php endif; ?>
>

<!-- Тело сайта, отвечает за вывод на страницу-->
<div class="wrapper">


    <style>
        header nav ul li.active .submenu a:hover, header nav ul li.active a, header nav ul li>a:hover{color: <?php echo e($modx->runSnippet('leo#colormenu')); ?>;}
    </style>
    <div class="content">
        <header style="<?php echo e($modx->runSnippet('leo#color')); ?>">
            <!-- Шапка сайта-->
            <div class="container">
                <div class="logo">
                    <a href="<?php echo e($modx->getConfig("site_url").$root); ?>">
                        <?php if($parentTemplate == 6): ?>
                            <img src="theme/img/goldef.svg">
                        <?php else: ?>
                            <img src="theme/img/golden-septic.png">
                        <?php endif; ?>
                    </a>
                </div>
                <nav>
                    <!-- Навигация -->
                    <input id="menu__toggle" type="checkbox" />
                    <label class="menu__btn" for="menu__toggle">
                        <span></span>
                    </label>

                    <ul class="topmenu">
                        <?php $__currentLoopData = $menutop; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(isset($menu['children'])): ?>
                                <?php if(isset($menu['active'])): ?>
                                <li class="level active"><a class="noclick" href="<?php echo e($root.$menu['url']); ?>" title="<?php echo e($menu['tv.pagetitle_'.$lang]); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?> <i class="fas fa-plus"></i></a>
                                    <ul class="submenu" style="background-color: <?php echo e($modx->runSnippet('leo#colormenu')); ?>">
                                        <?php $__currentLoopData = $menu['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $children): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li <?php echo e($data['classes']); ?>><a href="<?php echo e($root.$children['url']); ?>"><?php echo e($children['tv.pagetitle_'.$lang]); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </li>
                                    <?php else: ?>
                                    <li class="level <?php echo e($data['classes']); ?>"><a class="noclick" href="<?php echo e($root.$menu['url']); ?>" title="<?php echo e($menu['tv.pagetitle_'.$lang]); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?> <i class="fas fa-plus"></i></a>
                                        <ul class="submenu" style="background-color: <?php echo e($modx->runSnippet('leo#colormenu')); ?>">
                                            <?php $__currentLoopData = $menu['children']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $children): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li <?php echo e($data['classes']); ?>><a href="<?php echo e($root.$children['url']); ?>"><?php echo e($children['tv.pagetitle_'.$lang]); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </li>
                                    <?php endif; ?>
                            <?php elseif(isset($menu['here'])): ?>
                                <li class="active"><a href="<?php echo e($root.$menu['url']); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?></a></li>
                            <?php else: ?>
                                <li <?php echo e($data['classes']); ?>><a href="<?php echo e($root.$menu['url']); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <li <?php if($lang == 'ua'): ?>class="langli active"<?php else: ?> class="langli" <?php endif; ?>><a href="<?php echo e($modx->makeUrl($modx->documentIdentifier)); ?>">UA</a></li>
                            <li <?php if($lang == 'ru'): ?>class="langli active"<?php else: ?> class="langli" <?php endif; ?>><a href="<?php echo e('ru'.$modx->makeUrl($modx->documentIdentifier)); ?>">RU</a></li>
                            <li <?php if($lang == 'en'): ?>class="langli active"<?php else: ?> class="langli" <?php endif; ?>><a href="<?php echo e('en'.$modx->makeUrl($modx->documentIdentifier)); ?>">EN</a></li>
                    </ul>
                </nav>
                <ul class="langmenu">
                    <li <?php if($lang == 'ua'): ?>class="active"<?php endif; ?>><a href="<?php echo e($modx->makeUrl($modx->documentIdentifier)); ?>">UA</a></li>
                    <li>|</li>
                    <li <?php if($lang == 'ru'): ?>class="active"<?php endif; ?>><a href="<?php echo e('ru'.$modx->makeUrl($modx->documentIdentifier)); ?>">RU</a></li>
                    <li>|</li>
                    <li <?php if($lang == 'en'): ?>class="active"<?php endif; ?>><a href="<?php echo e('en'.$modx->makeUrl($modx->documentIdentifier)); ?>">EN</a></li>
                </ul>
            </div>
        </header>
        <?php if($documentObject['template'] == 6): ?>
            <div class=""><?php echo $__env->yieldContent('content'); ?></div>
        <?php else: ?>
            <?php echo $__env->yieldContent('content'); ?>
        <?php endif; ?>
    </div>
    <footer class="footer">
        <!-- Подвал сайта-->
        <div class="container">
            <div class="footer-contacts">
                <p><?php echo e($modx->getConfig('client_field_addres_'.$lang)); ?></p>
                <p>
                    <a href="tel:<?php echo e($modx->getConfig('client_field_phone1')); ?>"><?php echo e($modx->getConfig('client_field_phone1')); ?></a><br>
                    <a href="tel:<?php echo e($modx->getConfig('client_field_phone2')); ?>"><?php echo e($modx->getConfig('client_field_phone2')); ?></a><br>
                </p>
                <p><a href="mailto:<?php echo e($modx->getConfig('client_field_email')); ?>"><?php echo e($modx->getConfig('client_field_email')); ?></a></p>
                <p>© 2018-<?php echo e(date("Y")); ?> <?php echo e($modx->getConfig("__All_rights")); ?></p>
            </div>
            <div class="footer-menu">
                <ul>
                    <?php $__currentLoopData = $menubottom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e($root.$menu['url']); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div class="footer-menu">
                <ul>
                    <?php $__currentLoopData = $menufoot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e($root.$menu['url']); ?>"><?php echo e($menu['tv.pagetitle_'.$lang]); ?></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="copyright">
                <span><?php echo e($modx->getConfig("__copyright")); ?> <a href="//startpointstudio.com" target="_blank">startpointstudio</a></span>
            </div>
        </div>
    </footer>
</div>

<script type="text/javascript" src="theme/js/jquery-3.4.1.min.js"></script>

<?php echo $minifier->activate( [
    'theme/css/bootstrap/bootstrap.min.css',
    'theme/css/bootstrap/bootstrap-grid.min.css',
    'theme/css/bootstrap/bootstrap-reboot.min.css',
    'theme/css/owl-carousel/owl.carousel.min.css',
    'theme/css/owl-carousel/owl.theme.default.min.css',
    'theme/js/jquery.fancybox.min.css',
    'theme/js/lib/fontawesome/css/font-awesome.min.css',
    'theme/js/lib/aos/css/aos.css',
    'theme/css/style.css',
    'theme/WOW-master/css/libs/animate.css'
    ], $no_laravel_cache = 0, $minify = 1, $output_path = 'theme/css/' ); ?>



<script type="text/javascript" src="theme/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="theme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="theme/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="theme/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="theme/js/aos.js"></script>
<script defer src="theme/js/lib/fontawesome/js/all.js"></script>
<script type="text/javascript" src="theme/WOW-master/dist/wow.min.js"></script>
<script type="text/javascript" src="theme/js/script.js?v=3"></script>

<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVb8ogZLEPOizYq7VwjCEmCBB_8ZK-_mQ&amp;callback=initMap&language=<?php echo e($lang); ?>"></script>

<script>
    new WOW().init();
    var map, markers;
    function initMap() {
        var mapOptions = {
            center: new google.maps.LatLng(0, 0),
            scrollwheel:false,
            zoom:16,
            lang: 'ru'
        };
        var image = 'theme/img/marker.png';
        map = new google.maps.Map(document.getElementById('map-box-contact'),mapOptions);
        markers = [
            <?php echo e($modx->runSnippet('multiTV', [
            'tvName' => 'LatLng',
            'docid' => '4',
    ])); ?>

        ]
        var markersBounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            var markerPosition = markers[i].position;
            // Добавляем координаты маркера в область
            markersBounds.extend(markerPosition);
            var marker = new google.maps.Marker({
                position: markers[i].position,
                map: map,
                icon: image
            });
        }
        map.setCenter(markersBounds.getCenter());
    }


    $(document).on('submit','#ajaxForm form',function(ev){
        var frm = $('#ajaxForm form');
        $.ajax({
            type: 'post',
            url: '/feedbackajaxform',
            data: frm.serialize(),
            success: function (data) {
                $('ajaxForm form').remove();
                $('#ajaxForm').html( data );
            }
        });
        ev.preventDefault();
    });
</script>


</body>
</html><?php /**PATH /var/www/startpont/data/www/goldef.startpointstudio.com/views/layouts/main.blade.php ENDPATH**/ ?>