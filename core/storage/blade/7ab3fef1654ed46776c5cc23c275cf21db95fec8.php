<!DOCTYPE html>
<html lang="uk">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162514710-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162514710-1');
    </script>

    <?php $__env->startSection('keywords'); ?><?php echo evo_parser($documentObject['noIndex']);?><?php echo $__env->yieldSection(); ?>

    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo e($modx->getConfig("site_url")); ?>"/>

    <title>
        <?php $__env->startSection('title'); ?>
            <?php echo evo_parser($documentObject['titl']);?>
        <?php echo $__env->yieldSection(); ?>
    </title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <style>
        .page404 {
            text-align: center;
        }

        .page404 img {
            max-width: 100%;
        }
        
        .page404 .text {
            text-align: left;
            font-size: 30px;
            margin-left: auto;
            margin-right: auto;
            max-width: 690px;
            color: #737373;
            font-family: Arial, Helvetica;
        }

        .page404 .text ul li {
            font-size: 22px;
        }

        .page404 .text ul li a {
            color: inherit;
        }
    </style>
</head>
<body>
<div class="container page404">
    <img src="theme/img/page404.png" alt="" />
    
    <div class="text">
        Такой страницы не существует
        перейдите на другие страницы сайта:<br>
        <?php echo $modx->runSnippet('DLMenu', ['documents' => '1,2,3,4,5', 'showParent' => 1, 'maxDepth' => 1]); ?>

    </div>
</div>
</body>
</html><?php /**PATH /var/www/startpont/data/www/goldef.startpointstudio.com/views/page404.blade.php ENDPATH**/ ?>