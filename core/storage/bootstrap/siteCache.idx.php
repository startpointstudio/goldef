<?php
$c=&$this->config;$c['settings_version']="2.0.2";$c['manager_theme']="default";$c['server_offset_time']="0";$c['manager_language']="russian-UTF8";$c['modx_charset']="UTF-8";$c['site_name']="Goldef";$c['site_start']="1";$c['error_page']="56";$c['unauthorized_page']="1";$c['site_status']="1";$c['auto_template_logic']="parent";$c['default_template']="3";$c['old_template']="3";$c['publish_default']="1";$c['friendly_urls']="1";$c['friendly_alias_urls']="1";$c['use_alias_path']="1";$c['cache_type']="2";$c['failed_login_attempts']="3";$c['blocked_minutes']="60";$c['use_captcha']="0";$c['emailsender']="noreply@goldef.ua";$c['use_editor']="1";$c['use_browser']="1";$c['fe_editor_lang']="russian-UTF8";$c['fck_editor_toolbar']="standard";$c['fck_editor_autolang']="0";$c['editor_css_path']="";$c['editor_css_selectors']="";$c['upload_maxsize']="10485760";$c['manager_layout']="4";$c['auto_menuindex']="1";$c['session.cookie.lifetime']="604800";$c['mail_check_timeperiod']="600";$c['manager_direction']="ltr";$c['xhtml_urls']="0";$c['automatic_alias']="1";$c['datetime_format']="dd-mm-YYYY";$c['warning_visibility']="0";$c['remember_last_tab']="1";$c['enable_bindings']="1";$c['seostrict']="1";$c['number_of_results']="30";$c['theme_refresher']="";$c['show_picker']="0";$c['show_newresource_btn']="0";$c['show_fullscreen_btn']="0";$c['email_sender_method']="0";$c['site_id']="5dee03c69e04e";$c['a']="30";$c['site_unavailable_page']="";$c['reload_site_unavailable']="";$c['siteunavailable_message_default']="В настоящее время сайт недоступен.";$c['site_unavailable_message']="В настоящее время сайт недоступен.";$c['chunk_processor']="DLTemplate";$c['cache_default']="1";$c['search_default']="1";$c['custom_contenttype']="application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json";$c['docid_incrmnt_method']="0";$c['enable_cache']="1";$c['minifyphp_incache']="0";$c['server_protocol']="https";$c['rss_url_news']="http://feeds.feedburner.com/evocms-release-news";$c['track_visitors']="0";$c['friendly_url_prefix']="";$c['friendly_url_suffix']=".html";$c['make_folders']="0";$c['aliaslistingfolder']="0";$c['allow_duplicate_alias']="0";$c['manager_theme_mode']="3";$c['login_logo']="";$c['login_bg']="";$c['login_form_position']="left";$c['login_form_style']="dark";$c['manager_menu_position']="top";$c['tree_page_click']="27";$c['use_breadcrumbs']="0";$c['global_tabs']="1";$c['group_tvs']="5";$c['resource_tree_node_name']="pagetitle";$c['session_timeout']="15";$c['tree_show_protected']="0";$c['datepicker_offset']="-10";$c['number_of_logs']="100";$c['number_of_messages']="40";$c['which_editor']="TinyMCE4";$c['tinymce4_theme']="full";$c['tinymce4_skin']="lightgray";$c['tinymce4_skintheme']="inlite";$c['tinymce4_template_docs']="";$c['tinymce4_template_chunks']="";$c['tinymce4_entermode']="p";$c['tinymce4_element_format']="xhtml";$c['tinymce4_schema']="html5";$c['tinymce4_custom_plugins']="advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube";$c['tinymce4_custom_buttons1']="undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect";$c['tinymce4_custom_buttons2']="link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code formatselect";$c['tinymce4_custom_buttons3']="";$c['tinymce4_custom_buttons4']="";$c['tinymce4_blockFormats']="Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3";$c['use_udperms']="1";$c['udperms_allowroot']="0";$c['allow_eval']="with_scan";$c['safe_functions_at_eval']="time,date,strtotime,strftime";$c['check_files_onlogin']="index.php\r\n.htaccess\r\nmanager/index.php\r\n/core/config/database/connections/default.php";$c['validate_referer']="1";$c['rss_url_security']="http://feeds.feedburner.com/evocms-security-news";$c['error_reporting']="1";$c['send_errormail']="0";$c['pwd_hash_algo']="UNCRYPT";$c['reload_captcha_words']="";$c['captcha_words']="EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";$c['captcha_words_default']="EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";$c['filemanager_path']="/var/www/startpont/data/www/goldef.startpointstudio.com/";$c['upload_files']="bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF,svg,tpl";$c['upload_images']="bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,svg,webp";$c['upload_media']="au,avi,mp3,mp4,mpeg,mpg,wav,wmv";$c['upload_flash']="fla,flv,swf";$c['new_file_permissions']="0644";$c['new_folder_permissions']="0755";$c['which_browser']="mcpuk";$c['rb_webuser']="0";$c['rb_base_dir']="/var/www/startpont/data/www/goldef.startpointstudio.com/assets/";$c['rb_base_url']="assets/";$c['clean_uploaded_filename']="1";$c['strip_image_paths']="1";$c['maxImageWidth']="2600";$c['maxImageHeight']="2200";$c['clientResize']="0";$c['noThumbnailsRecreation']="0";$c['thumbWidth']="150";$c['thumbHeight']="150";$c['thumbsDir']=".thumbs";$c['jpegQuality']="90";$c['denyZipDownload']="0";$c['denyExtensionRename']="0";$c['showHiddenFiles']="0";$c['email_method']="mail";$c['smtp_auth']="0";$c['smtp_secure']="none";$c['smtp_host']="smtp.example.com";$c['smtp_port']="25";$c['smtp_username']="emailsender";$c['reload_emailsubject']="";$c['emailsubject_default']="Данные для авторизации";$c['emailsubject']="Данные для авторизации";$c['reload_websignupemail_message']="";$c['system_email_websignup_default']="Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";$c['websignupemail_message']="Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";$c['reload_system_email_webreminder_message']="";$c['system_email_webreminder_default']="Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";$c['webpwdreminder_message']="Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";$c['allow_multiple_emails']="0";$c['lang_code']="en";$c['sys_files_checksum']="a:4:{s:65:\"/var/www/startpont/data/www/goldef.startpointstudio.com/index.php\";s:32:\"6daef9ac899a7a01447baa236cf48eda\";s:65:\"/var/www/startpont/data/www/goldef.startpointstudio.com/.htaccess\";s:32:\"ef5d242f731ad45352035feee22d1e54\";s:73:\"/var/www/startpont/data/www/goldef.startpointstudio.com/manager/index.php\";s:32:\"a03fa8d6548de0562fdebcb67ba0e736\";s:101:\"/var/www/startpont/data/www/goldef.startpointstudio.com//core/config/database/connections/default.php\";s:32:\"770104cc68c96d3f9b0ad85057c40e1f\";}";$c['client_field_phone1']="+380 44 229 61 90";$c['client_field_phone2']="+380 67 240 38 59";$c['client_field_phone3']="";$c['client_field_addres']="08205, Київська область, м. Ірпінь, вул. Стельмаха, буд. 9 ";$c['client_field_email']="medipromtek@gmail.com";$c['client_field_addres_ua']="08205, Київська область, м. Ірпінь, вул. Стельмаха, буд. 9 ";$c['client_field_addres_ru']="08205, Киевска область, г. Ирпень, ул. Стельмаха, д. 9 ";$c['client_field_email_mail']="mail@wmlev.com";$c['client_field_addres_en']="08205, Київська область, м. Ірпінь, вул. Стельмаха, буд. 9 ";$this->aliasListing=array();$a=&$this->aliasListing;$d=&$this->documentListing;$m=&$this->documentMap;$a[1]=array('id'=>1,'alias'=>'index','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['index']=1;$m[]=array(0=>1);$a[62]=array('id'=>62,'alias'=>'kopiya-kompaniya-medipromtek','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['kopiya-kompaniya-medipromtek']=62;$m[]=array(0=>62);$a[2]=array('id'=>2,'alias'=>'preparaty','path'=>'','parent'=>0,'isfolder'=>1,'alias_visible'=>1);$d['preparaty']=2;$m[]=array(0=>2);$a[3]=array('id'=>3,'alias'=>'o-kompanii','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['o-kompanii']=3;$m[]=array(0=>3);$a[4]=array('id'=>4,'alias'=>'kontakty','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['kontakty']=4;$m[]=array(0=>4);$a[5]=array('id'=>5,'alias'=>'partnery','path'=>'','parent'=>0,'isfolder'=>1,'alias_visible'=>1);$d['partnery']=5;$m[]=array(0=>5);$a[14]=array('id'=>14,'alias'=>'otzyvy','path'=>'','parent'=>0,'isfolder'=>1,'alias_visible'=>1);$d['otzyvy']=14;$m[]=array(0=>14);$a[27]=array('id'=>27,'alias'=>'licenzii','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['licenzii']=27;$m[]=array(0=>27);$a[55]=array('id'=>55,'alias'=>'sitemap.xml','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['sitemap.xml']=55;$m[]=array(0=>55);$a[56]=array('id'=>56,'alias'=>'stranica-404','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['stranica-404']=56;$m[]=array(0=>56);$a[57]=array('id'=>57,'alias'=>'stati','path'=>'','parent'=>0,'isfolder'=>1,'alias_visible'=>1);$d['stati']=57;$m[]=array(0=>57);$a[60]=array('id'=>60,'alias'=>'antiseptis','path'=>'preparaty','parent'=>2,'isfolder'=>1,'alias_visible'=>1);$d['preparaty/antiseptis']=60;$m[]=array(2=>60);$a[6]=array('id'=>6,'alias'=>'golden-defence','path'=>'preparaty','parent'=>2,'isfolder'=>1,'alias_visible'=>1);$d['preparaty/golden-defence']=6;$m[]=array(2=>6);$a[7]=array('id'=>7,'alias'=>'silver-defence','path'=>'preparaty','parent'=>2,'isfolder'=>1,'alias_visible'=>1);$d['preparaty/silver-defence']=7;$m[]=array(2=>7);$a[8]=array('id'=>8,'alias'=>'ultra-protect','path'=>'preparaty','parent'=>2,'isfolder'=>1,'alias_visible'=>1);$d['preparaty/ultra-protect']=8;$m[]=array(2=>8);$a[54]=array('id'=>54,'alias'=>'vetpreparaty','path'=>'partnery','parent'=>5,'isfolder'=>0,'alias_visible'=>1);$d['partnery/vetpreparaty']=54;$m[]=array(5=>54);$a[9]=array('id'=>9,'alias'=>'vetmarket','path'=>'partnery','parent'=>5,'isfolder'=>0,'alias_visible'=>1);$d['partnery/vetmarket']=9;$m[]=array(5=>9);$a[47]=array('id'=>47,'alias'=>'golden-defence11','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence11']=47;$m[]=array(6=>47);$a[32]=array('id'=>32,'alias'=>'golden-defence2','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence2']=32;$m[]=array(6=>32);$a[67]=array('id'=>67,'alias'=>'sprej-protiparazitarnyj-golden-defence','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/sprej-protiparazitarnyj-golden-defence']=67;$m[]=array(6=>67);$a[48]=array('id'=>48,'alias'=>'golden-defence12','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence12']=48;$m[]=array(6=>48);$a[49]=array('id'=>49,'alias'=>'golden-defence13','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence13']=49;$m[]=array(6=>49);$a[19]=array('id'=>19,'alias'=>'tovar-1','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/tovar-1']=19;$m[]=array(6=>19);$a[37]=array('id'=>37,'alias'=>'golden-defence3','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence3']=37;$m[]=array(6=>37);$a[30]=array('id'=>30,'alias'=>'golden-defence1','path'=>'preparaty/golden-defence','parent'=>6,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/golden-defence/golden-defence1']=30;$m[]=array(6=>30);$a[36]=array('id'=>36,'alias'=>'golden-defence7','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/golden-defence7']=36;$m[]=array(7=>36);$a[41]=array('id'=>41,'alias'=>'silver-defence1','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/silver-defence1']=41;$m[]=array(7=>41);$a[42]=array('id'=>42,'alias'=>'silver-defence2','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/silver-defence2']=42;$m[]=array(7=>42);$a[35]=array('id'=>35,'alias'=>'golden-defence6','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/golden-defence6']=35;$m[]=array(7=>35);$a[43]=array('id'=>43,'alias'=>'silver-defence3','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/silver-defence3']=43;$m[]=array(7=>43);$a[44]=array('id'=>44,'alias'=>'silver-defence4','path'=>'preparaty/silver-defence','parent'=>7,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/silver-defence/silver-defence4']=44;$m[]=array(7=>44);$a[40]=array('id'=>40,'alias'=>'golden-defence10','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/golden-defence10']=40;$m[]=array(8=>40);$a[39]=array('id'=>39,'alias'=>'golden-defence9','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/golden-defence9']=39;$m[]=array(8=>39);$a[45]=array('id'=>45,'alias'=>'ultra-protect1','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/ultra-protect1']=45;$m[]=array(8=>45);$a[38]=array('id'=>38,'alias'=>'kopiya-kopiya-golden-defence','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/kopiya-kopiya-golden-defence']=38;$m[]=array(8=>38);$a[50]=array('id'=>50,'alias'=>'ultra-protect3','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/ultra-protect3']=50;$m[]=array(8=>50);$a[33]=array('id'=>33,'alias'=>'golden-defence8','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/golden-defence8']=33;$m[]=array(8=>33);$a[46]=array('id'=>46,'alias'=>'ultra-protect2','path'=>'preparaty/ultra-protect','parent'=>8,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/ultra-protect/ultra-protect2']=46;$m[]=array(8=>46);$a[16]=array('id'=>16,'alias'=>'kopiya-otzyv-1','path'=>'otzyvy','parent'=>14,'isfolder'=>0,'alias_visible'=>1);$d['otzyvy/kopiya-otzyv-1']=16;$m[]=array(14=>16);$a[17]=array('id'=>17,'alias'=>'kopiya-kopiya-otzyv-1','path'=>'otzyvy','parent'=>14,'isfolder'=>0,'alias_visible'=>1);$d['otzyvy/kopiya-kopiya-otzyv-1']=17;$m[]=array(14=>17);$a[18]=array('id'=>18,'alias'=>'kopiya-kopiya-kopiya-otzyv-1','path'=>'otzyvy','parent'=>14,'isfolder'=>0,'alias_visible'=>1);$d['otzyvy/kopiya-kopiya-kopiya-otzyv-1']=18;$m[]=array(14=>18);$a[15]=array('id'=>15,'alias'=>'otzyv-1','path'=>'otzyvy','parent'=>14,'isfolder'=>0,'alias_visible'=>1);$d['otzyvy/otzyv-1']=15;$m[]=array(14=>15);$a[58]=array('id'=>58,'alias'=>'statya-1','path'=>'stati','parent'=>57,'isfolder'=>0,'alias_visible'=>1);$d['stati/statya-1']=58;$m[]=array(57=>58);$a[59]=array('id'=>59,'alias'=>'kopiya-statya-1','path'=>'stati','parent'=>57,'isfolder'=>0,'alias_visible'=>1);$d['stati/kopiya-statya-1']=59;$m[]=array(57=>59);$a[61]=array('id'=>61,'alias'=>'antiseptic-80','path'=>'preparaty/antiseptis','parent'=>60,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/antiseptis/antiseptic-80']=61;$m[]=array(60=>61);$a[66]=array('id'=>66,'alias'=>'antiseptic-1000','path'=>'preparaty/antiseptis','parent'=>60,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/antiseptis/antiseptic-1000']=66;$m[]=array(60=>66);$a[63]=array('id'=>63,'alias'=>'chlorhexidine','path'=>'preparaty/antiseptis','parent'=>60,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/antiseptis/chlorhexidine']=63;$m[]=array(60=>63);$a[64]=array('id'=>64,'alias'=>'antiseptic-250','path'=>'preparaty/antiseptis','parent'=>60,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/antiseptis/antiseptic-250']=64;$m[]=array(60=>64);$a[65]=array('id'=>65,'alias'=>'antiseptic-500','path'=>'preparaty/antiseptis','parent'=>60,'isfolder'=>0,'alias_visible'=>1);$d['preparaty/antiseptis/antiseptic-500']=65;$m[]=array(60=>65);$c=&$this->contentTypes;$c['55']='text/xml';$c=&$this->chunkCache;$c['header']='<meta http-equiv="Content-Type" content="text/html; charset=[(modx_charset)]" /> 
<title>[*titl*]</title>
[*noIndex*]
<meta name="keywords" content="[*keyw*]" />
<meta name="description" content="[*desc*]" />
<base href="[(site_url)]"/>
';$c['head-Vd']='<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="js/lib/fancybox/jquery.fancybox.min.css">


    <link href="js/lib/fontawesome/css/solid.css" rel="stylesheet">
    <link href="js/lib/fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <link rel="stylesheet" href="css/style-main.css">
    <meta charset="utf-8"/>
    <title>Препараты</title>
</head>';$c['header-Vd']='<body>
<!-- Тело сайта, отвечает за вывод на страницу-->
<div class="wrapper">
    <section class="content">
        <header>
            <!-- Шапка сайта-->
            <div class="container">
                <div class="logo">
                    <a href="#"><img src="img/logo.png"></a>
                </div>
                <nav>
                    <!-- Навигация -->
                    <input id="menu__toggle" type="checkbox" />
                    <label class="menu__btn" for="menu__toggle">
                        <span></span>
                    </label>
                    <ul class="topmenu">
                        <li class="level">
                            <a href="#">Препарати<i class="fas fa-plus"></i></a>
                            <ul class="submenu">
                                <li>
                                    <a href="#">Golden Defence</a>
                                </li>
                                <li>
                                    <a href="#">Silver Defence</a>
                                </li>
                                <li>
                                    <a href="#">Ultra Protect</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">О компанії</a>
                        </li>
                        <li>
                            <a href="#">Контакти</a>
                        </li>
                        <li>
                            <a href="#">Партнери</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>';$c['mm_rules']='// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php
// example of how PHP is allowed - check that a TV named documentTags exists before creating rule

if ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getDatabase()->getFullTableName(\'site_tmplvars\'), "name=\'documentTags\'"))) {
	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV
}
mm_widget_showimagetvs(); // Always give a preview of Image TVs


//mm_createTab(\'Images\', \'photos\', \'\', \'\', \'\', \'850\');
//mm_moveFieldsToTab(\'images,photos\', \'photos\', \'\', \'\');

mm_hideFields(\'longtitle,description,link_attributes,menutitle,content,titl,keyw,desc,noIndex,left-content,right-content\', \'\', \'\');

//mm_hideTemplates(\'0,5,8,9,11,12\', \'2,3\');

//mm_hideTabs(\'settings, access\', \'2\');

mm_createTab(\'Украинский\', \'ua\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'pagetitle_ua,pagetitleonmain_ua,longtitle_ua,description_ua,content_ua,slider_ua,left-content_ua,right-content_ua,banner_ua,slide_cont_ua,slide_title_ua,kind_ua\', \'ua\', \'\', \'\');

mm_createTab(\'Русский\', \'ru\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'pagetitle_ru,pagetitleonmain_ru,longtitle_ru,description_ru,content_ru,slider_ru,left-content_ru,right-content_ru,banner_ru,slide_cont_ru,slide_title_ru,kind_ru\', \'ru\', \'\', \'\');

mm_createTab(\'Английский\', \'en\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'pagetitle_en,pagetitleonmain_en,longtitle_en,description_en,content_en,slider_en,left-content_en,right-content_en,banner_en,slide_cont_en,slide_title_en,kind_en\', \'en\', \'\', \'\');

mm_createTab(\'SEO (UA)\', \'seo_ua\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'titl_ua,keyw_ua,desc_ua,noIndex_ua\', \'seo_ua\', \'\', \'\');
mm_widget_tags(\'keyw_ua\',\',\');

mm_createTab(\'SEO (RU)\', \'seo_ru\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'titl_ru,keyw_ru,desc_ru,noIndex_ru\', \'seo_ru\', \'\', \'\');
mm_widget_tags(\'keyw_ru\',\',\');

mm_createTab(\'SEO (EN)\', \'seo_en\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'titl_en,keyw_en,desc_en,noIndex_en\', \'seo_en\', \'\', \'\');
mm_widget_tags(\'keyw_en\',\',\');
';$c['ru']=';category
placeholder_name			= \'Текст\';
';$s=&$this->snippetCache;$s['DLCrumbs']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLCrumbs.php\';';$s['DLCrumbsProps']='{"prefix":"client_"}';$s['DocInfo']='return require MODX_BASE_PATH.\'assets/snippets/docinfo/snippet.docinfo.php\';';$s['DocInfoProps']='{"prefix":"client_"}';$s['summary']='return require MODX_BASE_PATH.\'assets/snippets/summary/snippet.summary.php\';';$s['summaryProps']='{"prefix":"client_"}';$s['FormLister']='return require MODX_BASE_PATH.\'assets/snippets/FormLister/snippet.FormLister.php\';';$s['FormListerProps']='{"prefix":"client_"}';$s['DLSitemap']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLSitemap.php\';';$s['DLSitemapProps']='{"prefix":"client_"}';$s['phpthumb']='return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';';$s['phpthumbProps']='{"prefix":"client_"}';$s['DocLister']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DocLister.php\';';$s['DocListerProps']='{"prefix":"client_"}';$s['if']='return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';';$s['ifProps']='{"prefix":"client_"}';$s['DLMenu']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLMenu.php\';';$s['DLMenuProps']='{"prefix":"client_"}';$s['multiTV']='return require MODX_BASE_PATH . \'assets/tvs/multitv/multitv.snippet.php\';';$s['multiTVProps']='{"prefix":"client_"}';$s['AjaxEvo']='switch($_GET[\'q\']){
case \'feedbackajaxform\':
echo $modx->runSnippet(\'FormLister\', array(
                 \'formTpl\' => \'@B_FILE:partials/ContactForm\',
                                \'rules\' => \'{
                                "branch": {
                                  "required": "Введіть ім\\\'я"
                                },
                                "subject": {
                                  "required": "Введіть телефон або e-mail"
                                },
                                "text": {
                                  "required": "Введіть текст"
                                }
                                }\',
                                \'to\' => \'alexpavel55@gmail.com\',
                                \'formid\'=>\'basic\',
                                \'protectSubmit\' => \'0\',
                                \'submitLimit\' => \'0\',
                                \'subject\' => \'Связь с сайта\',
                                \'successTpl\' => \'@B_FILE:partials/SuccessTpl\',
                                \'messagesOuterTpl\' => \'@CODE: Ваше заявление успешно отправлено, в ближайшее время с Вами свяжуться\',
                                \'reportTpl\' => \'@CODE:
                                <p>Отделение: [+branch.value+]</p>
                                <p>Тема: [+subject.value+]</p>
                                <p>Сообщение: [+text.value+]</p>\',            ));
            die();
break;
}


 {!! $modx->runSnippet("FormLister", [
                                \'formTpl\' => \'@B_FILE:partials/ContactForm\',
                                \'rules\' => \'{
                                "branch": {
                                  "required": "Введіть ім\\\'я"
                                },
                                "subject": {
                                  "required": "Введіть телефон або e-mail"
                                },
                                "text": {
                                  "required": "Введіть текст"
                                }
                                }\',
                                \'to\' => \'alexpavel55@gmail.com\',
                                \'formid\'=>\'basic\',
                                \'protectSubmit\' => \'0\',
                                \'submitLimit\' => \'0\',
                                \'subject\' => \'Связь с сайта\',
                                \'successTpl\' => \'@B_FILE:partials/SuccessTpl\',
                                \'messagesOuterTpl\' => \'@CODE: Ваше заявление успешно отправлено, в ближайшее время с Вами свяжуться\',
                                \'reportTpl\' => \'@CODE:
                                <p>Отделение: [+branch.value+]</p>
                                <p>Тема: [+subject.value+]</p>
                                <p>Сообщение: [+text.value+]</p>\',
                            ]) !!}';$s['AjaxEvoProps']='{"prefix":"client_"}';$s['js']='if(!defined(\'MODX_BASE_PATH\')) {die(\'What are you doing? Get out of here!\');}

//параметры
$files = isset($files) ? $files : \'\'; // Список файлов (css, scss, less)
$minify = isset($minify) ? $minify : \'1\'; //сжымать и минифицировать файлы
$folder = isset($folder) ? $folder : \'\'; // папка для сгенерированных стилей по умолчанию в корень
//$inline = isset($inline) ? $inline : \'\'; // инлайн код стилей
//$parse = isset($parse) ? $parse : \'0\'; //обрабатывать ли теги MODX

//Обрабатываем файлы, преобразовываем less и scss
$filesArr = explode(\',\', str_replace(\'\\n\', \'\', $files));
foreach ($filesArr as $key => $value) {
	$file = MODX_BASE_PATH . trim($value);
	$v[$key] =  filemtime($file);
	$filesForMin[$key] = $file;  
}
if ($minify == \'1\') {
	include_once(MODX_BASE_PATH. "assets/snippets/cssjs/class.magic-min.php");
	$javascript = \'\';
	foreach($filesForMin as $file) {
			$javascript .= file_get_contents($file) . PHP_EOL;
	}
	$minified_javascript = \\JShrink\\Minifier::minify($javascript);
	file_put_contents(MODX_BASE_PATH.$folder.\'scripts.min.js\', $minified_javascript);
	return \'<script src="\'.$modx->config[\'site_url\'].$folder.\'scripts.min.js?v=\'.substr(md5(max($v)),0,3).\'"></script>\';
}

if ($minify == \'0\'){
	$links = \'\';
	foreach ($filesArr as $key => $value) {
		$links .= \'<script src="\'.$modx->config[\'site_url\'].trim($value).\'?v=\'.substr(md5($v[$key]),0,3).\'"></script>\';	
	}	
	return $links;
}

if ($minify == \'2\') {
	return \'<script src="\'.$modx->config[\'site_url\'].$folder.\'scripts.min.js?v=\'.substr(md5(max($v)),0,3).\'"></script>\';
}';$s['jsProps']='{"prefix":"client_"}';$s['css']='if(!defined(\'MODX_BASE_PATH\')) {die(\'What are you doing? Get out of here!\');}

//параметры
$files = isset($files) ? $files : \'\'; // Список файлов (css, scss, less)
$minify = isset($minify) ? $minify : \'1\'; //сжымать и минифицировать файлы
$folder = isset($folder) ? $folder : \'\'; // папка для сгенерированных стилей по умолчанию в корень
//$inline = isset($inline) ? $inline : \'\'; // инлайн код стилей
//$parse = isset($parse) ? $parse : \'0\'; //обрабатывать ли теги MODX

//Обрабатываем файлы, преобразовываем less и scss
$filesArr = explode(\',\', str_replace(\'\\n\', \'\', $files));
foreach ($filesArr as $key => $value) {
	$file = MODX_BASE_PATH . trim($value);
	$fileinfo = pathinfo($file);
	$v[$key] =  filemtime($file);
	switch ($fileinfo[\'extension\']) {
		case \'css\':
		$filesForMin[$key] = $file;  
		break;
		/*case \'less\':
		require_once(MODX_BASE_PATH. "assets/snippets/cssjs/less.inc.php"); 
		$less = new lessc;
		$less->checkedCompile($file, $folder.$fileinfo[\'filename\'].\'.css\');
		$filesForMin[$key] = $folder.$fileinfo[\'filename\'].\'.css\';
		break;*/
	}
}
if ($minify == \'1\') {
	include_once(MODX_BASE_PATH. "assets/snippets/cssjs/class.magic-min.php"); 
	$minified = new Minifier();
	$min = $minified->merge( MODX_BASE_PATH.$folder.\'styles.min.css\', \'css\', $filesForMin );
	return \'<link rel="stylesheet" href="\'.$modx->config[\'site_url\'].$folder.\'styles.min.css?v=\'.substr(md5(max($v)),0,3).\'" />\';
}else{
	$links = \'\';
	foreach ($filesArr as $key => $value) {
		$links .= \'<link rel="stylesheet" href="\'.$modx->config[\'site_url\'].trim($value).\'?v=\'.substr(md5($v[$key]),0,3).\'" />\';	
	}	
	return $links;
}';$s['cssProps']='{"prefix":"client_"}';$s['Breadcrubs']='if(!defined(\'MODX_BASE_PATH\')){die(\'What are you doing? Get out of here!\');}
( isset($maxCrumbs) ) ? $maxCrumbs : $maxCrumbs = 100;
( isset($pathThruUnPub) ) ? $pathThruUnPub : $pathThruUnPub = 1;
( isset($respectHidemenu) ) ? (int)$respectHidemenu : $respectHidemenu = 1;
( isset($showCurrentCrumb) ) ? $showCurrentCrumb : $showCurrentCrumb = 1;
( $currentAsLink ) ? $currentAsLink : $currentAsLink = 0;
( isset($linkTextField) ) ? $linkTextField : $linkTextField = \'menutitle,pagetitle,longtitle\';
( isset($linkDescField) ) ? $linkDescField : $linkDescField = \'description,longtitle,pagetitle,menutitle\';
( isset($showCrumbsAsLinks) ) ? $showCrumbsAsLinks : $showCrumbsAsLinks = 1;
( isset($templateSet) ) ? $templateSet : $templateSet = \'defaultString\';
( isset($crumbGap) ) ? $crumbGap : $crumbGap = \'...\';
( isset($stylePrefix) ) ? $stylePrefix : $stylePrefix = \'B_\';
( isset($showHomeCrumb) ) ? $showHomeCrumb : $showHomeCrumb = 1;
( isset($homeId) ) ? (int)$homeId : $homeId = $modx->config[\'site_start\'];
( isset($homeCrumbTitle) ) ? $homeCrumbTitle : $homeCrumbTitle = \'\';
( isset($homeCrumbDescription) ) ? $homeCrumbDescription : $homeCrumbDescription = \'\';
( isset($showCrumbsAtHome) ) ? $showCrumbsAtHome : $showCrumbsAtHome = 0;
( isset($hideOn) ) ? $hideOn : $hideOn = \'\';
( isset($hideUnder) ) ? $hideUnder : $hideUnder = \'\';
( isset($stopIds) ) ? $stopIds : $stopIds = \'\';
( isset($ignoreIds) ) ? $ignoreids : $ignoreids = \'\';
( isset($crumbSeparator) ) ? $separator = $crumbSeparator : $separator = \' &raquo; \';
( isset($separator) ) ? $separator : $separator = \' &raquo; \';
$templates = array(
    \'defaultString\' => array(
        \'crumb\' => \'[+crumb+]\',
        \'separator\' => \' \'.$separator.\' \',
        \'crumbContainer\' => \'<span class="[+crumbBoxClass+]">[+crumbs+]</span>\',
        \'lastCrumbWrapper\' => \'<span class="[+lastCrumbClass+]">[+lastCrumbSpanA+]</span>\',
        \'firstCrumbWrapper\' => \'<span class="[+firstCrumbClass+]">[+firstCrumbSpanA+]</span>\'
    ),
    \'defaultList\' => array(
        \'crumb\' => \'<li>[+crumb+]</li>\',
        \'separator\' => \'\',
        \'crumbContainer\' => \'<ul class="breadcrumb">[+crumbs+]</ul>\',
        \'lastCrumbWrapper\' => \'<li class="[+lastCrumbClass+]">[+lastCrumbSpanA+]</li>\',
        \'firstCrumbWrapper\' => \'<li class="[+firstCrumbClass+]">[+firstCrumbSpanA+]</li>\'
    ),
);
// Return blank if necessary: on home page
if ( !$showCrumbsAtHome && $homeId == $modx->documentObject[\'id\'] )
{
    return \'\';
}
// Return blank if necessary: specified pages
if ( $hideOn || $hideUnder )
{
    // Create array of hide pages
    $hideOn = str_replace(\' \',\'\',$hideOn);
    $hideOn = explode(\',\',$hideOn);

    // Get more hide pages based on parents if needed
    if ( $hideUnder )
    {
        $hiddenKids = array();
        // Get child pages to hide
        $hideKidsQuery = $modx->db->select(\'id\',$modx->getFullTableName("site_content"),"parent IN ($hideUnder)");
        while ( $hideKid = $modx->db->getRow($hideKidsQuery) )
        {
            $hiddenKids[] = $hideKid[\'id\'];
        }
        // Merge with hideOn pages
        $hideOn = array_merge($hideOn,$hiddenKids);
    }

    if ( in_array($modx->documentObject[\'id\'],$hideOn) )
    {
        return \'\';
    }

}
// Initialize ------------------------------------------------------------------
// Put certain parameters in arrays
$stopIds = str_replace(\' \',\'\',$stopIds);
$stopIds = explode(\',\',$stopIds);
$linkTextField = str_replace(\' \',\'\',$linkTextField);
$linkTextField = explode(\',\',$linkTextField);
$linkDescField = str_replace(\' \',\'\',$linkDescField);
$linkDescField = explode(\',\',$linkDescField);
$ignoreIds = str_replace(\' \',\'\',$ignoreIds);
$ignoreIds = explode(\',\',$ignoreIds);

/* $crumbs
 * Crumb elements are: id, parent, pagetitle, longtitle, menutitle, description,
 * published, hidemenu
 */
$crumbs = array();
$parent = $modx->documentObject[\'parent\'];
$output = \'\';
$maxCrumbs += ($showCurrentCrumb) ? 1 : 0;

// Replace || in snippet parameters that accept them with =
$crumbGap = str_replace(\'||\',\'=\',$crumbGap);

// Curent crumb ----------------------------------------------------------------

// Decide if current page is to be a crumb
if ( $showCurrentCrumb )
{
    $crumbs[] = array(
        \'id\' => $modx->documentObject[\'id\'],
        \'parent\' => $modx->documentObject[\'parent\'],
        \'pagetitle\' => $modx->documentObject[\'pagetitle\'],
        \'longtitle\' => $modx->documentObject[\'longtitle\'],
        \'menutitle\' => $modx->documentObject[\'menutitle\'],
        \'description\' => $modx->documentObject[\'description\']);
}

// Intermediate crumbs ---------------------------------------------------------


// Iterate through parents till we hit root or a reason to stop
$loopSafety = 0;
while ( $parent && $parent!=$modx->config[\'site_start\'] && $loopSafety < 1000 )
{
    // Get next crumb
    $tempCrumb = $modx->getPageInfo($parent,0,"id,parent,pagetitle,longtitle,menutitle,description,published,hidemenu");

    // Check for include conditions & add to crumbs
    if (
        $tempCrumb[\'published\'] &&
        ( !$tempCrumb[\'hidemenu\'] || !$respectHidemenu ) &&
        !in_array($tempCrumb[\'id\'],$ignoreIds)
    )
    {
        // Add crumb
        $crumbs[] = array(
        \'id\' => $tempCrumb[\'id\'],
        \'parent\' => $tempCrumb[\'parent\'],
        \'pagetitle\' => $tempCrumb[\'pagetitle\'],
        \'longtitle\' => $tempCrumb[\'longtitle\'],
        \'menutitle\' => $tempCrumb[\'menutitle\'],
        \'description\' => $tempCrumb[\'description\']);
    }

    // Check stop conditions
    if (
        in_array($tempCrumb[\'id\'],$stopIds) ||  // Is one of the stop IDs
        !$tempCrumb[\'parent\'] || // At root
        ( !$tempCrumb[\'published\'] && !$pathThruUnPub ) // Unpublished
    )
    {
        // Halt making crumbs
        break;
    }

    // Reset parent
    $parent = $tempCrumb[\'parent\'];

    // Increment loop safety
    $loopSafety++;
}

// Home crumb ------------------------------------------------------------------

if ( $showHomeCrumb && $homeId != $modx->documentObject[\'id\'] && $homeCrumb = $modx->getPageInfo($homeId,0,"id,parent,pagetitle,longtitle,menutitle,description,published,hidemenu") )
{
    $crumbs[] = array(
    \'id\' => $homeCrumb[\'id\'],
    \'parent\' => $homeCrumb[\'parent\'],
    \'pagetitle\' => $homeCrumb[\'pagetitle\'],
    \'longtitle\' => $homeCrumb[\'longtitle\'],
    \'menutitle\' => $homeCrumb[\'menutitle\'],
    \'description\' => $homeCrumb[\'description\']);
}


// Process each crumb ----------------------------------------------------------
$pretemplateCrumbs = array();

foreach ( $crumbs as $c )
{

    // Skip if we\'ve exceeded our crumb limit but we\'re waiting to get to home
    if ( count($pretemplateCrumbs) > $maxCrumbs && $c[\'id\'] != $homeId )
    {
        continue;
    }

    $text = \'\';
    $title = \'\';
    $pretemplateCrumb = \'\';

    // Determine appropriate span/link text: home link specified
    if ( $c[\'id\'] == $homeId && $homeCrumbTitle )
    {
        $text = $homeCrumbTitle;
    }
    else
    // Determine appropriate span/link text: home link not specified
    {
        for ($i = 0; !$text && $i < count($linkTextField); $i++)
        {
            if ( $c[$linkTextField[$i]] )
            {
                $text = $c[$linkTextField[$i]];
            }
        }
    }

    // Determine link/span class(es)
    if ( $c[\'id\'] == $homeId )
    {
        $crumbClass = $stylePrefix.\'homeCrumb\';
    }
    else if ( $modx->documentObject[\'id\'] == $c[\'id\'] )
    {
        $crumbClass = $stylePrefix.\'currentCrumb\';
    }
    else
    {
        $crumbClass = $stylePrefix.\'crumb\';
    }

    // Make link
    if (
        ( $c[\'id\'] != $modx->documentObject[\'id\'] && $showCrumbsAsLinks ) ||
        ( $c[\'id\'] == $modx->documentObject[\'id\'] && $currentAsLink )
    )
    {
        // Determine appropriate title for link: home link specified
        if ( $c[\'id\'] == $homeId && $homeCrumbDescription )
        {
            $title = htmlspecialchars($homeCrumbDescription);
        }
        else
        // Determine appropriate title for link: home link not specified
        {
            for ($i = 0; !$title && $i < count($linkDescField); $i++)
            {
                if ( $c[$linkDescField[$i]] )
                {
                    $title = htmlspecialchars($c[$linkDescField[$i]]);
                }
            }
        }


        $pretemplateCrumb .= \'<a class="\'.$crumbClass.\'" href="\'.($c[\'id\'] == $modx->config[\'site_start\'] ? $modx->config[\'base_url\'] : $modx->makeUrl($c[\'id\'])).\'" title="\'.$title.\'">[[DocInfo? &docid=`\'.$c[\'id\'].\'` &field=`pagetitle_[(_lang)]`]]\'.$ttext.\'</a>\';
    }
    else
    // Make a span instead of a link
    {
       $pretemplateCrumb .= \'<span class="\'.$crumbClass.\'">[[DocInfo? &docid=`\'.$c[\'id\'].\'` &field=`pagetitle_[(_lang)]`]]\'.$ttext.\'</span>\';
    }

    // Add crumb to pretemplate crumb array
    $pretemplateCrumbs[] = $pretemplateCrumb;

    // If we have hit the crumb limit
    if ( count($pretemplateCrumbs) == $maxCrumbs )
    {
        if ( count($crumbs) > ($maxCrumbs + (($showHomeCrumb) ? 1 : 0)) )
        {
            // Add gap
            $pretemplateCrumbs[] = \'<span class="\'.$stylePrefix.\'hideCrumb\'.\'">[[DocInfo? &docid=`\'.$c[\'id\'].\'` &field=`pagetitle_[(_lang)]`]]\'.$ccrumbGap.\'</span>\';
        }

        // Stop here if we\'re not looking for the home crumb
        if ( !$showHomeCrumb )
        {
            break;
        }
    }
}

// Put in correct order for output
$pretemplateCrumbs = array_reverse($pretemplateCrumbs);

// Wrap first/last spans
$pretemplateCrumbs[0] = str_replace(
    array(\'[+firstCrumbClass+]\',\'[+firstCrumbSpanA+]\'),
    array($stylePrefix.\'firstCrumb\',$pretemplateCrumbs[0]),
    $templates[$templateSet][\'firstCrumbWrapper\']
);
$pretemplateCrumbs[(count($pretemplateCrumbs)-1)] = str_replace(
    array(\'[+lastCrumbClass+]\',\'[+lastCrumbSpanA+]\'),
    array($stylePrefix.\'lastCrumb\',$pretemplateCrumbs[(count($pretemplateCrumbs)-1)]),
    $templates[$templateSet][\'lastCrumbWrapper\']
);

// Insert crumbs into crumb template
$processedCrumbs = array();
foreach ( $pretemplateCrumbs as $pc )
{
    $processedCrumbs[] = str_replace(\'[+crumb+]\',$pc,$templates[$templateSet][\'crumb\']);
}

// Combine crumbs together into one string with separator
$processedCrumbs = implode($templates[$templateSet][\'separator\'],$processedCrumbs);

// Put crumbs into crumb container template
$container = str_replace(
    array(\'[+crumbBoxClass+]\',\'[+crumbs+]\'),
    array($stylePrefix.\'crumbBox\',$processedCrumbs),
    $templates[$templateSet][\'crumbContainer\']
    );

// Return crumbs
return $container;';$s['BreadcrubsProps']='{"prefix":"client_"}';$s['switch']='$Mlang = LANG::GetInstance();
$lang=(string)$Mlang->lang;
if($pl) return \'[+\'.$pl.\'_\'.$lang.\'+]\';
if ($name) return \'[*\'.$name.\'_\'.$lang.\'*]\';
echo $$lang;';$s['switchProps']='{"prefix":"client_"}';$s['UltimateParent']='return require MODX_BASE_PATH.\'assets/snippets/ultimateparent/snippet.ultimateparent.php\';';$s['UltimateParentProps']='{"prefix":"client_"}';$p=&$this->pluginCache;$p['TransAlias']='require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';';$p['TransAliasProps']='{"prefix":"client_","table_name":"russian","char_restrict":"lowercase alphanumeric","remove_periods":"No","word_separator":"dash"}';$p['OutdatedExtrasCheck']='/**
 * OutdatedExtrasCheck
 *
 * Check for Outdated critical extras not compatible with EVO 1.4.6
 *
 * @category	plugin
 * @version     1.4.6
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     evo
 * @author      Author: Nicola Lambathakis
 * @internal    @events OnManagerWelcomeHome
 * @internal    @properties &wdgVisibility=Show widget for:;menu;All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly;AdminOnly &ThisRole=Run only for this role:;string;;;(role id) &ThisUser=Run only for this user:;string;;;(username)
 * @internal    @modx_category Manager and Admin
 * @internal    @installset base
 * @internal    @disabled 0
 */

require MODX_BASE_PATH . \'assets/plugins/extrascheck/OutdatedExtrasCheck.plugin.php\';';$p['OutdatedExtrasCheckProps']='{"prefix":"client_","wdgVisibility":"AdminOnly"}';$p['Forgot Manager Login']='require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';';$p['Forgot Manager LoginProps']='{"prefix":"client_"}';$p['CodeMirror']='/**
 * CodeMirror
 *
 * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)
 *
 * @category    plugin
 * @version     1.5
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     evo
 * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit
 * @internal    @modx_category Manager and Admin
 * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &darktheme=Dark Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;one-dark &fontSize=Font-size;list;10,11,12,13,14,15,16,17,18;14 &lineHeight=Line-height;list;1,1.1,1.2,1.3,1.4,1.5;1.3 &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250
 * @internal    @installset base
 * @reportissues https://github.com/evolution-cms/evolution/issues/
 * @documentation Official docs https://codemirror.net/doc/manual.html
 * @author      hansek from http://www.modxcms.cz
 * @author      update Mihanik71
 * @author      update Deesen
 * @author      update 64j
 * @lastupdate  08-01-2018
 */

$_CM_BASE = \'assets/plugins/codemirror/\';

$_CM_URL = MODX_SITE_URL . $_CM_BASE;

require(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');';$p['CodeMirrorProps']='{"prefix":"client_","theme":"default","darktheme":"one-dark","fontSize":"14","lineHeight":"1.3","indentUnit":"4","tabSize":"4","lineWrapping":"true","matchBrackets":"true","activeLine":"false","emmet":"true","search":"false","indentWithTabs":"true","undoDepth":"200","historyEventDelay":"1250"}';$p['TinyMCE4']='/**
 * TinyMCE4
 *
 * Javascript rich text editor
 *
 * @category    plugin
 * @version     4.7.4
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal    @properties &styleFormats=Custom Style Formats <b>RAW</b><br/><br/><ul><li>leave empty to use below block/inline formats</li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2</i></li><li>Also accepts full JSON-config as per TinyMCE4 docs / configure / content-formating / style_formats</li></ul>;textarea; &styleFormats_inline=Custom Style Formats <b>INLINE</b><br/><br/><ul><li>will wrap selected text with span-tag + css-class</li><li>simple-format only</li></ul>;textarea;InlineTitle,cssClass1|InlineTitle2,cssClass2 &styleFormats_block=Custom Style Formats <b>BLOCK</b><br/><br/><ul><li>will add css-class to selected block-element</li><li>simple-format only</li></ul>;textarea;BlockTitle,cssClass3|BlockTitle2,cssClass4 &customParams=Custom Parameters<br/><b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to "introtext";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline &browser_spellcheck=<b>Browser Spellcheck</b><br/>At least one dictionary must be installed inside your browser;list;enabled,disabled;disabled &paste_as_text=<b>Force Paste as Text</b>;list;enabled,disabled;disabled
 * @internal    @events OnLoadWebDocument,OnParseDocument,OnWebPagePrerender,OnLoadWebPageCache,OnRichTextEditorRegister,OnRichTextEditorInit,OnInterfaceSettingsRender
 * @internal    @modx_category Manager and Admin
 * @internal    @legacy_names TinyMCE Rich Text Editor
 * @internal    @installset base
 * @logo        /assets/plugins/tinymce4/tinymce/logo.png
 * @reportissues https://github.com/extras-evolution/tinymce4-for-modx-evo
 * @documentation Plugin docs https://github.com/extras-evolution/tinymce4-for-modx-evo
 * @documentation Official TinyMCE4-docs https://www.tinymce.com/docs/
 * @author      Deesen
 * @lastupdate  2018-01-17
 */
if (!defined(\'MODX_BASE_PATH\')) { die(\'What are you doing? Get out of here!\'); }

require(MODX_BASE_PATH."assets/plugins/tinymce4/plugin.tinymce.inc.php");';$p['TinyMCE4Props']='{"prefix":"client_","styleFormats_inline":"InlineTitle,cssClass1|InlineTitle2,cssClass2","styleFormats_block":"BlockTitle,cssClass3|BlockTitle2,cssClass4","entityEncoding":"named","pathOptions":"Site config","resizing":"false","webTheme":"webuser","webButtons1":"bold italic underline strikethrough removeformat alignleft aligncenter alignright","webButtons2":"link unlink image undo redo","webAlign":"ltr","width":"100%","height":"400px","introtextRte":"disabled","inlineMode":"disabled","inlineTheme":"inline","browser_spellcheck":"disabled","paste_as_text":"disabled"}';$p['ManagerManager']='// Run the main code
include MODX_BASE_PATH . \'assets/plugins/managermanager/mm.inc.php\';';$p['ManagerManagerProps']='{"prefix":"client_","remove_deprecated_tv_types_pref":"yes","config_chunk":"mm_rules"}';$p['AjaxEvo']='switch($_GET[\'q\']){
			
		case \'testtest\':	
			
			$str = file_get_contents("https://goldef.ua/");
			$matches = array();
			preg_match_all(\'/background[-image]*:.*[\\s]*url\\(["|\\\']+(.*)["|\\\']+\\)/\', $str, $matches, PREG_SET_ORDER);
			print_r($matches);
			
			die();
			break;
			
		case \'feedbackajaxform\':
			echo $modx->runSnippet(\'FormLister\', array(
				\'formTpl\' => \'@CODE:<form class="box-input-button-connection"  method="post">
					<input type="hidden" name="formid" value="basic">
					<div class="box-form">
						<div class="mistake-form-valid">
							<input name="branch" type="text" class="contact-form__input_name" placeholder="Ваше імя" value="[+branch.value+]">
							[+branch.error+]
							<input name="subject"type="text" class="contact-form__input_name" placeholder="Ваш телефон або електрона пошта" value="[+subject.value+]">
							[+subject.error+]
						</div>
						<div class="mistake-form-valid">
							<textarea name="text" id="text" type="text" rows="10" cols="10" wrap="hard" class="contact-form__input_name text-height-form-input" placeholder="Текст Вашого звернення" value="[+text.value+]"></textarea>
							[+text.error+]
						</div>
					</div>
					[+form.messages+]
					<button type="submit" class="send-form-connect-two">Відправити</button>
				</form>\',
				\'rules\' => \'{
                                "branch": {
                                  "required": "Введіть ім\\\'я"
                                },
                                "subject": {
                                  "required": "Введіть телефон або e-mail"
                                },
                                "text": {
                                  "required": "Введіть текст"
                                }
                                }\',
				\'to\' => \'alexpavel55@gmail.com\',
				\'formid\'=>\'basic\',
				\'protectSubmit\' => \'0\',
				\'submitLimit\' => \'0\',
				\'subject\' => \'Связь с сайта\',
				\'successTpl\' => \'@CODE:<div class="successTpl-wrap">
    <div class="successTpl">
        <div class="title">Ваше зверненні відправлено<br>Будь ласка зачекайьте, ми з вами звяжемось</div>
        <p>ok</p>
    </div>
</div>\',
				\'messagesOuterTpl\' => \'@CODE: Ваше заявление успешно отправлено, в ближайшее время с Вами свяжуться\',
				\'reportTpl\' => \'@CODE:
                                <p>Отделение: [+branch.value+]</p>
                                <p>Тема: [+subject.value+]</p>
                                <p>Сообщение: [+text.value+]</p>\',            ));
			die();
			break;
	}';$p['AjaxEvoProps']='{"prefix":"client_"}';$p['ClientSettings']='if ($modx->event->name == \'OnManagerMenuPrerender\') {
    require_once MODX_BASE_PATH . \'assets/modules/clientsettings/core/src/ClientSettings.php\';

    $cs   = new ClientSettings($params);
    $mid  = $cs->getModuleId();
    $lang = $cs->loadLang();
    $tabs = $cs->loadStructure();

    $menuparams = [\'client_settings\', \'main\', \'<i class="fa fa-cog"></i>\' . $lang[\'cs.module_title\'], \'index.php?a=112&id=\' . $mid, $lang[\'cs.module_title\'], \'\', \'\', \'main\', 0, 100, \'\'];

    if (count($tabs) > 1) {
        $menuparams[3] = \'javscript:;\';
        $menuparams[5] = \'return false;\';
        $sort = 0;

        $params[\'menu\'][\'client_settings_main\'] = [\'client_settings_main\', \'client_settings\', \'<i class="fa fa-cog"></i>\' . $lang[\'cs.module_title\'], \'index.php?a=112&id=\' . $mid, $lang[\'cs.module_title\'], \'\', \'\', \'main\', 0, $sort, \'\'];

        foreach ($tabs as $alias => $item) {
            if ($alias != \'default\') {
                $params[\'menu\'][\'client_settings_\' . $alias] = [\'client_settings_\' . $alias, \'client_settings\', \'<i class="fa \' . (isset($item[\'menu\'][\'icon\']) ? $item[\'menu\'][\'icon\'] : \'fa-cog\') . \'"></i>\' . $item[\'menu\'][\'caption\'], \'index.php?a=112&id=\' . $mid . \'&type=\' . $alias, $item[\'menu\'][\'caption\'], \'\', \'\', \'main\', 0, $sort += 10, \'\'];
            }
        }
    }

    $params[\'menu\'][\'client_settings\'] = $menuparams;
    $modx->event->output(serialize($params[\'menu\']));
    return;
}';$p['ClientSettingsProps']='{"prefix":"client_"}';$p['BLang']='/*
*	plugin BLang
*	From pack "Multilanguage"
*
*   Author: Bumkaka (find me at bumkaka.com)
*	skype: 	bumkaka2
*	email:  bumkaka@yandex.ru
*
*	date: 	14/06/2014
*	version:0.6
*/


if (!function_exists(\'prepareLang\')){
    function prepareLang($lang){
        switch ($lang){
            case \'ua\':
                $lang = \'uk\';
                break;
        }
        return $lang;
    }
}
if (!function_exists(\'crawlerDetect\')){
	function crawlerDetect(){
		$BotList = array(\'Googlebot\', \'Baiduspider\', \'ia_archiver\',
        \'R6_FeedFetcher\', \'NetcraftSurveyAgent\',
        \'bingbot\', \'facebookexternalhit\', \'PrintfulBot\',
        \'msnbot\', \'Twitterbot\', \'UnwindFetchor\',
        \'urlresolver\', \'Butterfly\', \'TweetmemeBot\', \'tweet\',\'Yammybot\', \'Openbot\', \'Yahoo\', \'Slurp\',  \'Lycos\',  \'AltaVista\', \'Teoma\', \'Gigabot\', \'Googlebot-Mobile\', "alexa", "froogle",  "inktomi", "looksmart", "URL_Spider_SQL", "Firefly",
						 "NationalDirectory", "Ask","Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler",
						 "www.galaxy.com", "Googlebot/2.1", "Google", "Scooter", "James Bond",
						  "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz", "Feedfetcher-Google",
						 "TechnoratiSnoop", "Rankivabot", "Mediapartners-Google", "Sogou","spider", "MJ12bot",
						 "Yandex/", "YaDirectBot", "StackRambler","DotBot","dotbot","LinkedInBot");
		$agent = strtolower($_SERVER[\'HTTP_USER_AGENT\']);
		foreach($BotList as $bot) {
			if(strpos(  $agent , strtolower($bot) ) !==false) {
				return $bot;
			}
		}
		return false;
	}
}

$e =& $modx->event;
$modx->config[\'agent\'] = $_SERVER[\'HTTP_USER_AGENT\'];

$fields = isset($fields)?$fields:\'\';
$translate = isset($translate)?$translate:0;
$languages = isset($languages)?explode(\',\',$languages):\'\';
$root = isset($root)?$root:\'ua\';
$yandexKey = isset($yandexKey)?$yandexKey:\'\';

if ($e->name == \'OnDocFormSave\' && $translate == 1 ) {

require_once MODX_BASE_PATH.\'assets/modules/blang/translate/src/Translation.php\';
require_once MODX_BASE_PATH.\'assets/modules/blang/translate/src/Translator.php\';
require_once MODX_BASE_PATH.\'assets/modules/blang/translate/src/Exception.php\';
require_once MODX_BASE_PATH.\'assets/lib/MODxAPI/modResource.php\';



$sql = "select * from ".$modx->getFullTableName(\'site_tmplvars\');
$q = $modx->db->query($sql);
$resp = $modx->db->makeArray($q);

$tvs = [];
foreach ($resp as $re) {

$tvs[$re[\'name\']] = \'tv\'.$re[\'id\'];
}


$data = [];
$fields = explode(\',\',$fields);



if(empty($_POST[$tvs[\'pagetitle_\'.$root]]) && !empty($_POST[\'pagetitle\'])){
$_POST[$tvs[\'pagetitle_\'.$root]] = $_POST[\'pagetitle\'];
$data[\'pagetitle_\'.$root] = $_POST[\'pagetitle\'];
}

$translator = new Yandex\\Translate\\Translator($yandexKey);

if(is_array($languages)){
foreach ($languages as $lang) {
if($lang == $root){ continue;}
foreach ($fields as $field) {


if(empty($_POST[$tvs[$field.\'_\'.$root]]) || !empty($_POST[$tvs[$field.\'_\'.$lang]])){continue;}

$translation = $translator->translate($_POST[$tvs[$field.\'_\'.$root]], prepareLang($root) . \'-\' . prepareLang($lang));
$data[$field.\'_\'.$lang] = (string) $translation;
}
}
}
$doc = new modResource($modx);
$doc->edit((int) $id);
foreach ($data as $key => $value) {
$doc->set($key,$value);
}
return $doc->save(false,false);
}
if ($e->name == \'OnLoadWebDocument\' && !crawlerDetect()) {
$l = explode(\'-\',$_SERVER[\'HTTP_ACCEPT_LANGUAGE\'])	;


if ( empty( $_COOKIE[\'deflang\'])){
if ( strtolower( $l[0] ) !=\'ru\'){
$_GET[\'lang\'] = \'ua\';
} else {
$_GET[\'lang\'] = \'ru\';
}
setcookie(\'deflang\', $_GET[\'lang\'] , time() + 10000000, \'/\');
$modx->sendRedirect( \'http://\'.$_SERVER[\'HTTP_HOST\'].\'/\'.($_GET[\'lang\']==\'ru\'?\'ru/\'.$_GET[\'q\']:$_GET[\'q\']) );

}
}

$languageUrls = [];
foreach ($languages as $language) {
if($language == $root){
$languageUrls[$language] = \'\';
}
else{
$languageUrls[$language] = $language.\'/\';
}
}
require_once MODX_BASE_PATH.\'assets/modules/blang/lang.class.inc.php\';
$Lang = LANG::GetInstance($root,$languages, $languageUrls);
$lang=(string)$Lang->lang;

$access = $modx->getTemplateVar(\'access_\'.$lang, \'*\', $modx->documentIdentifier);
if ($access[\'value\']==\'0\') $this->sendForward( $modx->config[\'site_url\'].$modx->config[\'_root\'], \'HTTP/1.0 404 Not Found\');


/*
* for bAuth
*/
$_SESSION[\'_root\'] = $modx->config[\'_root\'];';$p['BLangProps']='{"prefix":"client_","fields":"pagetitle,longtitle,menutitle,introtext,content","translate":"0","languages":"ru,ua,en","root":"ua"}';$e=&$this->pluginEvent;$e['OnBeforeDocFormSave']=array('ManagerManager');$e['OnBeforeManagerLogin']=array('Forgot Manager Login');$e['OnChunkFormRender']=array('CodeMirror');$e['OnDocDuplicate']=array('ManagerManager');$e['OnDocFormPrerender']=array('ManagerManager');$e['OnDocFormRender']=array('CodeMirror','ManagerManager');$e['OnDocFormSave']=array('BLang','ManagerManager');$e['OnInterfaceSettingsRender']=array('TinyMCE4');$e['OnLoadWebDocument']=array('TinyMCE4','BLang');$e['OnLoadWebPageCache']=array('TinyMCE4','BLang');$e['OnManagerAuthentication']=array('Forgot Manager Login');$e['OnManagerLoginFormRender']=array('Forgot Manager Login');$e['OnManagerMenuPrerender']=array('ClientSettings');$e['OnManagerWelcomeHome']=array('OutdatedExtrasCheck');$e['OnModFormRender']=array('CodeMirror');$e['OnPageNotFound']=array('AjaxEvo');$e['OnParseDocument']=array('TinyMCE4');$e['OnPluginFormRender']=array('CodeMirror','ManagerManager');$e['OnRichTextEditorInit']=array('CodeMirror','TinyMCE4');$e['OnRichTextEditorRegister']=array('TinyMCE4');$e['OnSnipFormRender']=array('CodeMirror');$e['OnStripAlias']=array('TransAlias');$e['OnTempFormRender']=array('CodeMirror');$e['OnTVFormRender']=array('ManagerManager');$e['OnWebPageInit']=array('BLang');$e['OnWebPagePrerender']=array('TinyMCE4');
