<?php
if ($modx->documentIdentifier == 2) {
    $url = $modx->makeUrl(6);

    $Mlang = LANG::GetInstance();
    $lang=(string)$Mlang->lang;

    if($lang != 'ua') {
        $url = '/'.$lang.$url;
    }

    $modx->sendRedirect($url);
}

return;