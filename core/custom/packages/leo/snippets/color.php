<?php
if ($modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'color-head-top'])!='') {
    $topcolor = $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'color-head-top']);
    $bottomcolor = $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'color-head-bottom']);
} else {
    foreach ( $modx->getDocument($modx->documentIdentifier, 'parent') as $id){
        $topcolor = $modx->runSnippet('DocInfo', ['docid' => $id, 'field' => 'color-head-top']);
        $bottomcolor = $modx->runSnippet('DocInfo', ['docid' => $id, 'field' => 'color-head-bottom']);
    }
}
    $output = 'background: linear-gradient(0deg, ' . $bottomcolor . ' 0%, ' . $topcolor . ' 100%);';
return $output;