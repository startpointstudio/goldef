<?php
if ($modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'color-head-top'])!='') {
    $topcolor = $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'color-head-bottom']);
} else {
    foreach ( $modx->getDocument($modx->documentIdentifier, 'parent') as $id){
        $topcolor = $modx->runSnippet('DocInfo', ['docid' => $id, 'field' => 'color-head-bottom']);
    }
}
return $topcolor;