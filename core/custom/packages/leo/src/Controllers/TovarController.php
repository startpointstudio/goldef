<?php
namespace EvolutionCMS\Leo\Controllers;

class TovarController extends BaseController
{
    function nocacheRender()
    {
        $modx = EvolutionCMS();
        $this->data['otherCatalog'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => $modx->documentObject['parent'] , 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,kind']), true);
        $this->data['initiation'] = json_decode($this->evo->runSnippet('multiTV', ['toJson' => 1, 'tvName' =>'initiation', 'display' => 'all', 'docid' => $modx->documentIdentifier]), true);
        $this->data['bgcolorInitiation'] = 'background-color:'.$this->evo->runSnippet('DocInfo', ['docid' => $modx->documentObject['parent'], 'field' =>'bgcolor-initiation']);
        $this->data['bgcolorContent'] = 'background-color:'.$this->evo->runSnippet('DocInfo', ['docid' => $modx->documentObject['parent'], 'field' =>'bgcolor-content']);
    }
}