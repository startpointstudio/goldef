<?php
namespace EvolutionCMS\Leo\Controllers;

class CatalogantiseptikController extends BaseController
{
    function nocacheRender()  {

        $this->data['sliderCatalog'] = json_decode($this->evo->runSnippet('multiTV', ['toJson' => 1, 'tvName' =>'slider_'.$this->data['lang'], 'display' => 'all', 'docid' => $this->evo->documentIdentifier]), true);

        $this->data['catalog'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => 60, 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en']), true);

    }

}