<?php
namespace EvolutionCMS\Leo\Controllers;

class Home2Controller extends BaseController
{
    function nocacheRenderTemplate() {

        $this->data['productsOnMain'] = json_decode($this->evo->runSnippet('DocLister', ['documents' => $this->evo->documentObject['products_on_main'][1], 'api' => 1, 'sortType' => 'doclist', 'tvList' => 'img,img_en,pagetitle_'.$this->data['lang'].',pagetitleonmain_'.$this->data['lang']]), true);

        $this->data['catalogAntiseptik'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '60', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en,pagetitle_'.$lang]), true);

        $this->data['dogCatalogIndexg'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '6', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en,kind', 'filters' => 'AND(tv:kind:=:собака)']), true);

        $this->data['dogCatalogIndexs'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '7', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en,kind', 'filters' => 'AND(tv:kind:=:собака)']), true);

        $this->data['dogCatalogIndexu'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '8', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en,kind', 'filters' => 'AND(tv:kind:=:собака)']), true);

        $this->data['catCatalogIndex'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '6,7,8', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,img_en,kind', 'filters' => 'AND(tv:kind:=:кошка)']), true);
    }
}