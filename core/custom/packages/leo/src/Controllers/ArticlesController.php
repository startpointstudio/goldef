<?php
namespace EvolutionCMS\Leo\Controllers;

class ArticlesController extends BaseController
{
    function nocacheRender()
    {
        $this->data['articles'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => $this->evo->documentObject['id'] , 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,pagetitle_'.$this->data['lang'], 'summary' => 'content']), true);

    }
}