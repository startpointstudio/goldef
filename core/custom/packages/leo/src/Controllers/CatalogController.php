<?php
namespace EvolutionCMS\Leo\Controllers;

class CatalogController extends BaseController
{
    function nocacheRender()  {

        $this->data['sliderCatalog'] = json_decode($this->evo->runSnippet('multiTV', ['toJson' => 1, 'tvName' =>'slider_'.$this->data['lang'], 'display' => 'all', 'docid' => $this->evo->documentIdentifier]), true);

        $this->data['dogCatalog'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => ''.$documentObject['id'].'', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,kind,img_en', 'filters' => 'AND(tv:kind:=:собака)']), true);

        $this->data['catCatalog'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => ''.$documentObject['id'].'', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,kind,img_en', 'filters' => 'AND(tv:kind:=:кошка)']), true);

    }

}