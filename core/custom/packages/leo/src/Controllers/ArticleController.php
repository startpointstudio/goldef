<?php
namespace EvolutionCMS\Leo\Controllers;

class ArticleController extends BaseController
{
    function nocacheRender()
    {
        $this->data['imgbig'] = $this->evo->runSnippet("phpthumb", array('input' => $this->evo->documentObject['img_big'][1], 'options' => 'w=1600,h=370,q=100,zc=1'));
    }
}