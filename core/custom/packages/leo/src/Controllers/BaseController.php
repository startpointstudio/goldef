<?php

namespace EvolutionCMS\Leo\Controllers;

use Illuminate\Support\Facades\Cache;
//Базовый класс который занимается обработкой/кэшированием и прочими вещами.
class BaseController
{
    public $data = [];

    public function __construct()
    {
        $this->evo = EvolutionCMS();
        ksort($_GET);
        $cacheid = md5(json_encode($_GET));
        $this->noCacheRenderGlobal();
        if ($this->evo->getConfig('enable_cache')) {
            $this->data = Cache::rememberForever($cacheid, function () {
                $this->globalElements();
                $this->render();
                return $this->data;
            });
        } else {
            $this->globalElements();
            $this->render();
        }
        $this->noCacheRender();
        $this->nocacheRenderTemplate();
        $this->sendToView();
    }

    public function render() {

        $this->data['menufoot'] = json_decode($this->evo->runSnippet('DLMenu', ['documents' => '3,5,27', 'showParent' => 1, 'tvList' => 'pagetitle_'.$this->data['lang'], 'api' => 1]), true)[0];

        $this->data['menubottom'] = json_decode($this->evo->runSnippet('DLMenu', ['parents' => '2', 'showParent' => 0, 'tvList' => 'pagetitle_'.$this->data['lang'], 'api' => 1]), true)[0];

        $this->data['goods'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '2', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img', 'display' => 3, 'total' => 3]), true);

        $this->data['goodsTovar'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '6', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img']), true);

        $this->data['reviews'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '14', 'api' => 1, 'order' => 'ASC', 'tvList' => 'img,content_'.$this->data['lang'] ]), true);

        $this->data['partners'] = json_decode($this->evo->runSnippet('DocLister', ['parents' => '5', 'api' => 1, 'order' => 'menuindex ASC', 'tvList' => 'img,partnerslink']), true);

        $this->data['slider'] = json_decode($this->evo->runSnippet('multiTV', ['toJson' => 1, 'tvName' =>'slider_'.$this->data['lang'], 'display' => 'all', 'docid' => '1']), true);



    }

    public function noCacheRenderGlobal() {

        if(isset($_GET['lang']) && ($_GET['lang'] == 'ru')) {
            $lang = 'ru';
            $root = 'ru';
        }
        elseif(isset($_GET['lang']) && ($_GET['lang'] == 'en')) {
            $lang = 'en';
            $root = 'en';
        }
        else {
            $lang = 'ua';
            $root = '';
        }


        $this->data['lang'] = $lang;
        $this->data['root'] = $root;

        $this->data['menutop'] = json_decode($this->evo->runSnippet('DLMenu', ['parents' => '2,3,4,5', 'showParent' => 1, 'tvList' => 'pagetitle_'.$this->data['lang'], 'api' => '1']), true)[0];

        $ultimateParent = $this->evo->runSnippet("UltimateParent", array('topLevel' => 2));
        $this->data['parentTemplate'] = $this->evo->runSnippet("DocInfo", array('docid' => $ultimateParent, 'field' => 'template'));

    }

    public function noCacheRender() {

    }

    public function nocacheRenderTemplate() {

    }

    public function globalElements() {

    }

    public function sendToView()
    {
        $this->evo->addDataToView($this->data);
    }
}