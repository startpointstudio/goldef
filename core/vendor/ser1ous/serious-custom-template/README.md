# seriousCustomTemplate
Custom template service provider for evo 2.0

## Install

1) `php artisan package:installrequire evocms-ser1ous/serious-custom-template '*'` in you **core/** folder
2) `php artisan sct:install` 

## Установка  

1) `php artisan package:installrequire evocms-ser1ous/serious-custom-template '*'` в папке **core/**
2) `php artisan sct:install`
