// $(window).load(function(){
// 	$(".catalog .owlItem .section-tovar h1").css({"display":"block"});
// });

$(document).ready(function(){

	$(".table-end-text iframe").parent().css({display: "block",textAlign: "center"});

	setTimeout(function() {
		$('.catalog-productov').find('.owlItem .section-tovar h1').removeClass("hide").fadeIn(3000);
	}, 1500);
	setInterval(function() {
		if($('.owl-item').hasClass("active")) {
			setTimeout(function() {
				$('.owl-item.active').find('.owlItem .text-left').addClass("active").fadeTo(1000);
			}, 2500);
		} else {
		}
	}, 10);
	setInterval(function() {
		$(".owl-item:not(.active)").find('.owlItem .text-left').removeClass("active");
	}, 1);
  	$(".slider-wrap .owl-carousel").owlCarousel({
		items:1,
		nav:true,
		navText: false,
		dots:false,
		loop:true,
		autoplay:true,
		autoplayTimeout: 5000,
		smartSpeed: 1000,
		responsive:{
			0:{
				nav:false
			},
			600:{
				nav:true,
			}
		}
  	});
	$(".tab-content .owl-carousel").owlCarousel({
		nav:false,
		navText: false,
		dots:true,
		autoplay:true,
		autoplayTimeout: 3000,
		smartSpeed: 1000,
		responsive:{
			0:{
				items:2,
				nav:false
			},
			600:{
				items:4,
				nav:true
			},
			1000:{
				items:5
			},
			1200: {
				items:7
			}
		},
		// onDragged: callback
	});
	function callback(event) {
		// Provided by the core
		var element   = event.target;         // DOM element, in this example .owl-carousel
		var name      = event.type;           // Name of the event, in this example dragged
		var namespace = event.namespace;      // Namespace of the event, in this example owl.carousel
		var items     = event.item.count;     // Number of items
		var item      = event.item.index;     // Position of the current item
		// Provided by the navigation plugin
		var pages     = event.page.count;     // Number of pages
		var page      = event.page.index;     // Position of the current page
		var size      = event.page.size;      // Number of items per page
		if (items<7){
			owl.trigger('destroy.owl.carousel');
		}
	}
	$(".rewievs-wrap .owl-carousel").owlCarousel({
		items:3,
		nav:true,
		navText: false,
		dots:true,
		loop:true,
		autoplay:true,
		autoplayTimeout: 5000,
		smartSpeed: 1000,
		responsive:{
			0:{
				items:1,
				nav:false
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
	$(".partners .owl-carousel").owlCarousel({
		items:4,
		nav:true,
		navText: false,
		dots:true,
		autoplayTimeout: 2000,
		smartSpeed: 1000,
		autoplay:true,
		center:true,
		responsive:{
			0:{
				items:1,
				nav:false
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		}
	});
	$(".center-produkt-cat .owl-carousel").owlCarousel({
		nav:false,
		navText: false,
		dots:true,
		loop:true,
		autoplayTimeout: 2000,
		smartSpeed: 1000,
		autoplay:true,
		responsive:{
			0:{
				items:1,
				nav:false
			},
			480:{
				items:2,
				nav:false
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:4,
				nav:false
			},
			1200: {
				items:6,
				nav:false
			}
		}
	});
	setTimeout(function () {
		$(".tab-content").removeClass("hide");
	}, 1000);
	var owl = $('.owl-carousel');
	$('.owl-prev').click(function() {
		owl.trigger('stop.owl.autoplay');
		setTimeout(function () {
			owl.trigger('play.owl.autoplay');
		}, 1000);
	});
	$('.owl-next').click(function() {
		owl.trigger('stop.owl.autoplay');
		setTimeout(function () {
			owl.trigger('play.owl.autoplay');
		}, 1000);
	});
	$(".level a").click(function(event) {
		if ($(this).hasClass('noclick')){
			event.preventDefault();
			$(this).removeClass('noclick').addClass('act').closest('.level').find('.submenu').slideToggle();
		}else {
			window.location = this.href;
		}
	});
	$('.successTpl-wrap p').click(function(event) {
		$(this).closest('.successTpl-wrap').addClass('hide');
	});
	$('[data-fancybox="images"]').fancybox({
		helpers : {
			overlay : {
				locked : false // try changing to true and scrolling around the page
			}
		},
		scrolling : 'yes'
	});
	$(".tobtn").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top-40;
		$('body,html').animate({scrollTop: top}, 1500);
	});

	var win = $(window);
	var dog = $('.dog-produkt-wrap');
	var cat = $('.cat-produkt-wrap');
	var mql2 = window.matchMedia('all and (max-width: 480px)');
	if((dog.length != 0) || (cat.length != 0)) {
		if (mql2.matches) {	
			$('.dog-produkt-wrap .goodsItem-produkt').removeClass('hide');
			$(".cat-produkt-wrap .goodsItem-produkt").removeClass('hide');
		}else {

			win.scroll(function() {
				if(win.scrollTop() + win.height() >= dog.offset().top+400) {
					$(".dog-produkt-wrap .goodsItem-produkt").each(function (i) {
						$(this).delay((i++) * 300).removeClass('hide').addClass('active');
					});
				}else{
					$(".dog-produkt-wrap .goodsItem-produkt").each(function (i) {
						$(this).delay((i++) * 300).removeClass('active').addClass('hide');
					});
				}
			});
			win.scroll(function() {
				if(win.scrollTop() + win.height() >= cat.offset().top+400) {
					$(".cat-produkt-wrap .goodsItem-produkt").each(function (i) {
						$(this).delay((i++) * 300).removeClass('hide').addClass('active');
					});
				}else{
					$(".cat-produkt-wrap .goodsItem-produkt").each(function (i) {
						$(this).delay((i++) * 300).removeClass('active').addClass('hide');
					});
				}
			});
		}
	}

});
$(document).on("scroll",function(){
	if($(document).scrollTop()>72){
		$("header").removeClass("large").addClass("small");
	}
	else{
		$("header").removeClass("small").addClass("large");
	}
});
$(window).scroll(function() {
	if ($(this).scrollTop() > 200) {
		setTimeout(function () {
			$(".goodsItem.two").addClass("active");
		}, 200);
		setTimeout(function () {
			$(".goodsItem.one").addClass("active");
		}, 450);
		setTimeout(function () {
			$(".goodsItem.three").addClass("active");
		}, 600);
	}
	if ($(this).scrollTop() > 400) {
		setTimeout(function () {
			$(".goodsItem.two").addClass("active");
		}, 200);
		setTimeout(function () {
			$(".goodsItem.one").addClass("active");
		}, 450);
		setTimeout(function () {
			$(".goodsItem.three").addClass("active");
		}, 600);
	}
	if ($(this).scrollTop() > 500) {
		$(".tadItem").each(function (i) {
			$(this).delay((i++) * 150).removeClass('hide').fadeTo(200, 1);
		});
	}
});
console.log(0);
AOS.init();
$('.owlItem').filter(":visible").addClass('dhjsbdg');
setTimeout(function() {
	$('.catalog-productov').find('.owlItem .section-tovar>img').removeClass("hide").fadeTo(1000);
}, 500);
setTimeout(function() {
	$('.catalog-productov').find('.owlItem .text-left').removeClass("hide").fadeTo(1000);
}, 1500);


