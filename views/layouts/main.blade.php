<!DOCTYPE html>
<html lang="uk">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162514710-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162514710-1');
    </script>

    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@section('keywords')@evoParser($documentObject['keyw_'.$lang])@show">
    <meta name="description" content="@section('description')@evoParser($documentObject['desc_'.$lang])@show">
    <base href="{{ $modx->getConfig("site_url") }}"/>

    <link href="theme/css/all.css" rel="stylesheet">

    @if ($documentObject['template'] != 4)
        <link rel="stylesheet" href="theme/css/style-main.css?v=4">
    @endif
    <title>
        @section('title')
            @evoParser($documentObject['titl_'.$lang])
        @show
    </title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<body
@if ($documentObject['template'] == 9)
    class="contact-wrap"
@elseif ($documentObject['template'] == 10 )
    class="partners-wrap"
@elseif ($documentObject['template'] == 11 )
    class="partners-wrap about"
@elseif ($documentObject['template'] != 1)
    class="bg-wrapp"
@endif
>

<!-- Тело сайта, отвечает за вывод на страницу-->
<div class="wrapper">


    <style>
        header nav ul li.active .submenu a:hover, header nav ul li.active a, header nav ul li>a:hover{color: {{$modx->runSnippet('leo#colormenu')}};}
    </style>
    <div class="content">
        <header style="{{$modx->runSnippet('leo#color')}}">
            <!-- Шапка сайта-->
            <div class="container">
                <div class="logo">
                    <a href="{{ $modx->getConfig("site_url").$root }}">
                        @if($parentTemplate == 6)
                            <img src="theme/img/goldef.svg">
                        @else
                            <img src="theme/img/golden-septic.png">
                        @endif
                    </a>
                </div>
                <nav>
                    <!-- Навигация -->
                    <input id="menu__toggle" type="checkbox" />
                    <label class="menu__btn" for="menu__toggle">
                        <span></span>
                    </label>

                    <ul class="topmenu">
                        @foreach($menutop as $menu)
                            @if(isset($menu['children']))
                                @if (isset($menu['active']))
                                <li class="level active"><a class="noclick" href="{{$root.$menu['url']}}" title="{{$menu['tv.pagetitle_'.$lang]}}">{{$menu['tv.pagetitle_'.$lang]}} <i class="fas fa-plus"></i></a>
                                    <ul class="submenu" style="background-color: {{$modx->runSnippet('leo#colormenu')}}">
                                        @foreach($menu['children'] as $children)
                                            <li {{$data['classes']}}><a href="{{$root.$children['url']}}">{{$children['tv.pagetitle_'.$lang]}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                    @else
                                    <li class="level {{$data['classes']}}"><a class="noclick" href="{{$root.$menu['url']}}" title="{{$menu['tv.pagetitle_'.$lang]}}">{{$menu['tv.pagetitle_'.$lang]}} <i class="fas fa-plus"></i></a>
                                        <ul class="submenu" style="background-color: {{$modx->runSnippet('leo#colormenu')}}">
                                            @foreach($menu['children'] as $children)
                                                <li {{$data['classes']}}><a href="{{$root.$children['url']}}">{{$children['tv.pagetitle_'.$lang]}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endif
                            @elseif (isset($menu['here']))
                                <li class="active"><a href="{{$root.$menu['url']}}">{{$menu['tv.pagetitle_'.$lang]}}</a></li>
                            @else
                                <li {{$data['classes']}}><a href="{{$root.$menu['url']}}">{{$menu['tv.pagetitle_'.$lang]}}</a></li>
                            @endif
                        @endforeach
                            <li @if($lang == 'ua')class="langli active"@else class="langli" @endif><a href="{{ $modx->makeUrl($modx->documentIdentifier) }}">UA</a></li>
                            <li @if($lang == 'ru')class="langli active"@else class="langli" @endif><a href="{{ 'ru'.$modx->makeUrl($modx->documentIdentifier) }}">RU</a></li>
                            <li @if($lang == 'en')class="langli active"@else class="langli" @endif><a href="{{ 'en'.$modx->makeUrl($modx->documentIdentifier) }}">EN</a></li>
                    </ul>
                </nav>
                <ul class="langmenu">
                    <li @if($lang == 'ua')class="active"@endif><a href="{{ $modx->makeUrl($modx->documentIdentifier) }}">UA</a></li>
                    <li>|</li>
                    <li @if($lang == 'ru')class="active"@endif><a href="{{ 'ru'.$modx->makeUrl($modx->documentIdentifier) }}">RU</a></li>
                    <li>|</li>
                    <li @if($lang == 'en')class="active"@endif><a href="{{ 'en'.$modx->makeUrl($modx->documentIdentifier) }}">EN</a></li>
                </ul>
            </div>
        </header>
        @if ($documentObject['template'] == 6)
            <div class="">@yield('content')</div>
        @else
            @yield('content')
        @endif
    </div>
    <footer class="footer">
        <!-- Подвал сайта-->
        <div class="container">
            <div class="footer-contacts">
                <p>{{$modx->getConfig('client_field_addres_'.$lang)}}</p>
                <p>
                    <a href="tel:{{$modx->getConfig('client_field_phone1')}}">{{$modx->getConfig('client_field_phone1')}}</a><br>
                    <a href="tel:{{$modx->getConfig('client_field_phone2')}}">{{$modx->getConfig('client_field_phone2')}}</a><br>
                </p>
                <p><a href="mailto:{{$modx->getConfig('client_field_email')}}">{{$modx->getConfig('client_field_email')}}</a></p>
                <p>© 2018-{{date("Y")}} {{ $modx->getConfig("__All_rights") }}</p>
            </div>
            <div class="footer-menu">
                <ul>
                    @foreach($menubottom as $menu)
                        <li><a href="{{$root.$menu['url']}}">{{$menu['tv.pagetitle_'.$lang]}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="footer-menu">
                <ul>
                    @foreach($menufoot as $menu)
                        <li><a href="{{$root.$menu['url']}}">{{$menu['tv.pagetitle_'.$lang]}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="clear"></div>
            <div class="copyright">
                <span>{{ $modx->getConfig("__copyright") }} <a href="//startpointstudio.com" target="_blank">startpointstudio</a></span>
            </div>
        </div>
    </footer>
</div>

<script type="text/javascript" src="theme/js/jquery-3.4.1.min.js"></script>

{!! $minifier->activate( [
    'theme/css/bootstrap/bootstrap.min.css',
    'theme/css/bootstrap/bootstrap-grid.min.css',
    'theme/css/bootstrap/bootstrap-reboot.min.css',
    'theme/css/owl-carousel/owl.carousel.min.css',
    'theme/css/owl-carousel/owl.theme.default.min.css',
    'theme/js/jquery.fancybox.min.css',
    'theme/js/lib/fontawesome/css/font-awesome.min.css',
    'theme/js/lib/aos/css/aos.css',
    'theme/css/style.css',
    'theme/WOW-master/css/libs/animate.css'
    ], $no_laravel_cache = 0, $minify = 1, $output_path = 'theme/css/' ) !!}


<script type="text/javascript" src="theme/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="theme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="theme/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="theme/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="theme/js/aos.js"></script>
<script defer src="theme/js/lib/fontawesome/js/all.js"></script>
<script type="text/javascript" src="theme/WOW-master/dist/wow.min.js"></script>
<script type="text/javascript" src="theme/js/script.js?v=3"></script>

<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVb8ogZLEPOizYq7VwjCEmCBB_8ZK-_mQ&amp;callback=initMap&language={{$lang}}"></script>

<script>
    new WOW().init();
    var map, markers;
    function initMap() {
        var mapOptions = {
            center: new google.maps.LatLng(0, 0),
            scrollwheel:false,
            zoom:16,
            lang: 'ru'
        };
        var image = 'theme/img/marker.png';
        map = new google.maps.Map(document.getElementById('map-box-contact'),mapOptions);
        markers = [
            {{$modx->runSnippet('multiTV', [
            'tvName' => 'LatLng',
            'docid' => '4',
    ])}}
        ]
        var markersBounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            var markerPosition = markers[i].position;
            // Добавляем координаты маркера в область
            markersBounds.extend(markerPosition);
            var marker = new google.maps.Marker({
                position: markers[i].position,
                map: map,
                icon: image
            });
        }
        map.setCenter(markersBounds.getCenter());
    }


    $(document).on('submit','#ajaxForm form',function(ev){
        var frm = $('#ajaxForm form');
        $.ajax({
            type: 'post',
            url: '/feedbackajaxform',
            data: frm.serialize(),
            success: function (data) {
                $('ajaxForm form').remove();
                $('#ajaxForm').html( data );
            }
        });
        ev.preventDefault();
    });
</script>


</body>
</html>