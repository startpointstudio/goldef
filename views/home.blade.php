﻿@extends('layouts.main')

@section('content')
    <section class="slider-wrap home">
            <div class="hide">{{$a=1}}</div>
        <div class="owl-carousel">

            @foreach($slider as $slide)
                @if($a==1)
                <div class="owlItem {{ $slide['id'] }}" style="background-image: url('{{$slide['img']}}'); background-image: url('{{$slide['img_webp']}}')">
                    <div class="container">
                        <div class="section-tovar">
                            <img  class="" src="{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']) !!}" alt="" >
                        </div>
                        <div class="text-left active">
                            <p>{{$slide['text']}}</p>
                        </div>
                    </div>
                    <div class="hide">{{$a=$a+1}}</div>
                </div>
                @else
                    <div class="owlItem {{$slide['id']}}" style="background-image: url('{{$slide['img']}}'); background-image: url('{{$slide['img_webp']}}')">
                        <div class="container">
                            <div class="section-tovar">
                                <img  class="hide" src="{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']) !!}" alt="" >
                            </div>
                            <div class="text-left">
                                <p>{{$slide['text']}}</p>
                            </div>
                        </div>
                    <div class="hide">{{$a=$a+1}}</div>
                    </div>
                @endif
            @endforeach
        </div>
    </section>
    <section class="goodsItem-wrap">
        <div class="container">
            <div class="row">
                @foreach($goods as $goodValue)
                <div class="col-md-4  col-sm-6">
                    @if($goodValue['id']==8)
                        <div class="goodsItem three" data-wow-duration="0s" data-wow-delay="0">
                    @elseif($goodValue['id']==7)
                        <div class="goodsItem one" data-wow-duration="0.5s" data-wow-delay="0.5">
                    @else
                        <div class="goodsItem two" data-wow-duration="1s" data-wow-delay="1">
                    @endif
                            <a href="{{$root.$goodValue['url']}}"><img src="{{$goodValue['tv_img']}}"></a>
                            <a href="{{$root.$goodValue['url']}}" class="title">{{$goodValue['pagetitle']}}</a>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
            <div class="title">{{ $modx->getConfig("__Choose_protection") }}</div>
            <div class="tabs">
                <div class="container">
                    <div class="tab tab1">
                        <input type="radio" id="tab1" name="tab-group" checked>
                        <label for="tab1" class="tab-title one"></label>
                        <section class="tab-content">
                            <div class="container">
                                <div class="owl-carousel owl-theme">
                                    @foreach($dogCatalogIndexg as $goodsItemg)
                                        <div class="tadItem hide">
                                            <a href="{{$root.$goodsItemg['url']}}">
                                                <img src="{{$goodsItemg['tv_img']}}">
                                            </a>
                                        </div>
                                    @endforeach
                                        @foreach($dogCatalogIndexs as $goodsItems)
                                            <div class="tadItem hide">
                                                <a href="{{$root.$goodsItems['url']}}">
                                                    <img src="{{$goodsItems['tv_img']}}">
                                                </a>
                                            </div>
                                        @endforeach
                                        @foreach($dogCatalogIndexu as $goodsItemu)
                                            <div class="tadItem hide">
                                                <a href="{{$root.$goodsItemu['url']}}">
                                                    <img src="{{$goodsItemu['tv_img']}}">
                                                </a>
                                            </div>
                                        @endforeach
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab tab2">
                        <input type="radio" id="tab2" name="tab-group">
                        <label for="tab2" class="tab-title two"></label>
                        <section class="tab-content">
                            <div class="container">
                                <div class="owl-carousel owl-theme">
                                    @foreach($catCatalogIndex as $goodsItem)
                                        <div class="tadItem hide">
                                            <a href="{{$root.$goodsItem['url']}}">
                                                <img src="{{$goodsItem['tv_img']}}">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
    </section>
    @include("partials.rewievsBlock")
    @include("partials.videoRewievs")
    @include("partials.partnersBlock")
@endsection
