<section class="partners">
    <div class="container">
        @if($documentObject['id'] != '5')
        <div class="blue-title">{{ $modx->getConfig("__Our_partners") }}</div>
        @else
            <h1 class="blue-title">{{ $modx->getConfig("__Our_partners") }}</h1>
        @endif
        <div class="owl-carousel owl-theme">
            @foreach($partners as $partner)
                <div class="partnersItem">
                    <div class="img">
						<a href="{{$partner['tv_partnerslink']}}" target="_blanc"><img src="{{$partner['tv_img']}}" alt=""></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>