<form class="box-input-button-connection"  method="post">
    <input type="hidden" name="formid" value="basic">
    <div class="box-form">
        <div class="mistake-form-valid">
            <div class="form-item">
                <input id="name" name="name" type="text" class="contact-form__input_name" placeholder="{{ $modx->getConfig("__form_name") }}" value="{{ $data["name.value"] }}">
                {!! $data["name.error"] !!}
            </div>
            <div class="form-item">
                <input id="contact" name="contact" type="text" class="contact-form__input_name" placeholder="{{ $modx->getConfig("__form_phone") }}" value="{{ $data["contact.value"] }}">
                {!! $data["contact.error"] !!}
            </div>
        </div>
        <div class="mistake-form-valid textarea">
            <textarea name="text" id="text" type="text" rows="10" cols="10" wrap="hard" class="contact-form__input_name text-height-form-input" placeholder="{{ $modx->getConfig("__form_text") }}" value="{{ $data["text.value"] }}"></textarea>
            {!! $data["text.error"] !!}
        </div>
    </div>
    {!! $data["form.messages"] !!}
    <button type="submit" class="send-form-connect-two">{{ $modx->getConfig("__Send") }}</button>
</form>