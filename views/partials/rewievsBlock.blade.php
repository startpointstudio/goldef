<section class="rewievs-wrap">
    <div class="container">
        <div class="blue-title">{{ $modx->getConfig("__Reviews") }}</div>
        <div class="rewievsItems">
            <div class="owl-carousel owl-theme">
            {{$a=1}}
            @foreach($reviews as $review)
                @if($a==1)
                    <div class="rewievsItem first">
                @elseif($a>2)
                    <div class="rewievsItem last">
                @else
                    <div class="rewievsItem">
                @endif
                        <img data-aos="zoom-in" src="{{$review['tv_img']}}">
                        <div data-aos="fade-up" > {!! $review['tv_content_'.$lang] !!} </div>
                    </div>
                    {{$a=$a+1}}
                @endforeach
            </div>
        </div>
    </div>
</section>