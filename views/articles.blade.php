@extends('layouts.main')

@section('content')
    <section class="slider-wrap">
        <div class="img-main-section"><img src="{!! $modx->runSnippet('DocInfo', ['docid' => $documentObject['id'], 'field' => 'img', 'render' => '1']) !!}" alt=""></div>
    </section>
    <section class="articles">
        <div class="container">

            <h1>{{ $documentObject['pagetitle_'.$lang] }}</h1>

            <ul>
                @foreach($articles as $item)
                <li class="tile">
                    <div class="img visible">
                        <img alt="{{ $item['tv_pagetitle_'.$lang] }}" src="{{ $item['tv_img'] }}" class="taller">
                    </div>
                    <div class="description">
                        <div class="h3like">{{ $item['tv_pagetitle_'.$lang] }}</div>
                    </div>
                    <a class="item-link view" href="{{ $modx->makeUrl($item['id']) }}">{{ $modx->getConfig("__More") }}</a>
                </li>
                @endforeach
            </ul>
        </div>
    </section>

    <div class="clear"></div>

    @include("partials.partnersBlock")

@endsection
