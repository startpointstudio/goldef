@extends('layouts.main')

@section('content')
    <section class="slider-wrap">
        <div class="owl-carousel">
            {{--        <div class="owlItem"><img src="theme/img/slide.jpg"></div>--}}
            {{--        <div class="owlItem"><img src="theme/img/slide.jpg"></div>--}}
            {{--        <div class="owlItem"><img src="theme/img/slide.jpg"></div>--}}
            {{$partners}}
        </div>
    </section>
    <section class="goodsItem-wrap">
        <div class="container">
            <div class="row">
                @foreach($goods as $goodValue)
                    <div class="col-md-4  col-sm-6">
                        <div class="goodsItem three">
                            <a href="{{$goodValue['url']}}"><img src="{{$goodValue['tv_img']}}"></a>
                            <a href="{{$goodValue['url']}}" class="title">{{$goodValue['pagetitle']}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="title">ВИБЕРІТЬ ЗАХИСТ ДЛЯ ВАШОЇ ТВАРИНИ </div>
        <div class="tabs">
            <div class="container">
                <div class="tab">
                    <input type="radio" id="tab1" name="tab-group" checked>
                    <label for="tab1" class="tab-title one"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel">
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab">
                    <input type="radio" id="tab2" name="tab-group">
                    <label for="tab2" class="tab-title two"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel">
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                                <div class="tadItem">
                                    <a href="#">
                                        <img src="theme/img/goods-small.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <section class="rewievs-wrap">
        <div class="container">
            <div class="blue-title">ЩО КАЖУТЬ ПРО НАС ЛЮДИ</div>
            <div class="rewievsItems">
                <div class="owl-carousel">
                    <div class="rewievsItem">
                        <img src="theme/img/rew.png">
                        <p>Я цілий день працюю, сидячи за комп'ютером, внаслідок чого часто почала боліти шия. Купила собі кінський фітокрем Palladium з охолоджуючою і протитравматичною</p>
                    </div>
                    <div class="rewievsItem">
                        <img src="theme/img/rew.png">
                        <p>Я цілий день працюю, сидячи за комп'ютером, внаслідок чого часто почала боліти шия. Купила собі кінський фітокрем Palladium з охолоджуючою і протитравматичною</p>
                    </div>
                    <div class="rewievsItem">
                        <img src="theme/img/rew.png">
                        <p>Я цілий день працюю, сидячи за комп'ютером, внаслідок чого часто почала боліти шия. Купила собі кінський фітокрем Palladium з охолоджуючою і протитравматичною</p>
                    </div>
                    <div class="rewievsItem">
                        <img src="theme/img/rew.png">
                        <p>Я цілий день працюю, сидячи за комп'ютером, внаслідок чого часто почала боліти шия. Купила собі кінський фітокрем Palladium з охолоджуючою і протитравматичною</p>
                    </div>
                    <div class="rewievsItem">
                        <img src="theme/img/rew.png">
                        <p>Я цілий день працюю, сидячи за комп'ютером, внаслідок чого часто почала боліти шия. Купила собі кінський фітокрем Palladium з охолоджуючою і протитравматичною</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="video-rewievs-wrap">
        <div class="container">
            <div class="row">
                <div class="video-rewies-text">
                    <div class="title">
                        Врач Галина Iванiвна Погорелова про Golden Defence
                    </div>
                    <p>
                        Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила п'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.
                    </p>
                </div>
                <div class="video-rewies">
                    <a data-fancybox="" href="https://www.youtube.com/watch?v=AXAGF7L9wP8" target="_blank">
                        <picture>  <img srcset="theme/img/video.jpg" alt="Видеo"> </picture>
                        <span><i class="far fa-play-circle"></i></span>
                    </a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <section class="partners">
        <div class="container">
            <div class="blue-title">НАШІ ПАРТНЕРИ</div>
            <div class="owl-carousel">
                @foreach($partners as $partner)
                    <div class="partnersItem">
                        <div class="img">
                            <img src="{{$partner['tv_img']}}" alt="">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
