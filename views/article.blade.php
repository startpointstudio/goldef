@extends('layouts.main')

@section('content')
    <section class="slider-wrap">
        <div class="img-main-section"><img src="{!! $imgbig !!}" alt=""></div>
    </section>
    <section class="article">
        <div class="container">

            <h1>{{ $documentObject['pagetitle_'.$lang] }}</h1>

            {!! $documentObject['content_'.$lang] !!}

        </div>
    </section>

    <div class="clear"></div>

    @include("partials.partnersBlock")

@endsection
