@extends('layouts.main')

@section('content')
    <section class="slider-wrap home">
        <div class="hide">{{$a=1}}</div>
        <div class="owl-carousel">

            @foreach($slider as $slide)
                @if($a==1)
                    <div class="owlItem {{$slide['id']}}" style="background-image: url('{{$slide['img']}}'); background-image: url('{{$slide['img_webp']}}')">
                        <div class="container">
                            <div class="section-tovar">
                                <img  class="" src="{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']) !!}" alt="" >
                                {{--                            <a  class="hide" href="[~{{$slide['id']}}~]">{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'pagetitle']) !!}</a>--}}
                            </div>
                            <div class="text-left active">
                                <p>{{$slide['text']}}</p>
                            </div>
                        </div>
                        <div class="hide">{{$a=$a+1}}</div>
                    </div>
                @else
                    <div class="owlItem {{$slide['id']}}" style="background-image: url('{{$slide['img']}}'); background-image: url('{{$slide['img_webp']}}')">
                        <div class="container">
                            <div class="section-tovar">
                                <img  class="hide" src="{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']) !!}" alt="" >
                                {{--                            <a  class="hide" href="[~{{$slide['id']}}~]">{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'pagetitle']) !!}</a>--}}
                            </div>
                            <div class="text-left">
                                <p>{{$slide['text']}}</p>
                            </div>
                        </div>
                        <div class="hide">{{$a=$a+1}}</div>
                    </div>
                @endif
            @endforeach
        </div>
    </section>

    <section class="icons-wrap text-center">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-recycle"></i>
                        </div>
                        <div class="ico-title">
                            {{ $modx->getConfig("__Full_production_cycle") }}
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fab fa-battle-net"></i>
                        </div>
                        <div class="ico-title">
                            {{ $modx->getConfig("__Innovative_technologies") }}
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-globe-americas"></i>
                        </div>
                        <div class="ico-title">
                            {{ $modx->getConfig("__World_recognition") }}
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="icon-item">
                        <div class="icon-img">
                            <i class="fas fa-chart-line"></i>
                        </div>
                        <div class="ico-title">
                            {{ $modx->getConfig("__Efficiency") }}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="goodsItem-wrap">
        <div class="container">
            <div class="row">

                @foreach($productsOnMain as $product)

                    <div class="product_item col-md-3">
                        <div class="product_img">
                            <a href="{{$root.$modx->makeUrl($product['id'])}}"><img src="@if($lang == 'en'){!! $product['tv_img_en'] !!}@else{!! $product['tv_img'] !!}@endif" alt="{{$product['tv_pagetitleonmain_'.$lang]}}"></a>
                        </div>
                        <div class="product_title">
                            <a href="{{$root.$modx->makeUrl($product['id'])}}">{{$product['tv_pagetitleonmain_'.$lang]}}</a>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </section>

    <section class="goodsItem-wrap">
        <div class="tabs">
            <div class="container">
                <div class="tab tab3">
                    <input type="radio" id="tab3" name="tab-group" checked>
                    <label for="tab3" class="tab-title three"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                @foreach($catalogAntiseptik as $goodsItem)
                                    <div class="tadItem hide">
                                        <a href="{{$root.$goodsItem['url']}}">
                                            <img src="@if($lang == 'en'){!! $goodsItem['tv_img_en'] !!}@else{!! $goodsItem['tv_img'] !!}@endif">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab tab1">
                    <input type="radio" id="tab1" name="tab-group">
                    <label for="tab1" class="tab-title one"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                @foreach($dogCatalogIndexg as $goodsItemg)
                                    <div class="tadItem hide">
                                        <a href="{{$root.$goodsItemg['url']}}">
                                            <img src="@if($lang == 'en'){!! $goodsItemg['tv_img_en'] !!}@else{!! $goodsItemg['tv_img'] !!}@endif">
                                        </a>
                                    </div>
                                @endforeach
                                @foreach($dogCatalogIndexs as $goodsItems)
                                    <div class="tadItem hide">
                                        <a href="{{$root.$goodsItems['url']}}">
                                            <img src="@if($lang == 'en'){!! $goodsItems['tv_img_en'] !!}@else{!! $goodsItems['tv_img'] !!}@endif">
                                        </a>
                                    </div>
                                @endforeach
                                @foreach($dogCatalogIndexu as $goodsItemu)
                                    <div class="tadItem hide">
                                        <a href="{{$root.$goodsItemu['url']}}">
                                            <img src="@if($lang == 'en'){!! $goodsItemu['tv_img_en'] !!}@else{!! $goodsItemu['tv_img'] !!}@endif">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab tab2">
                    <input type="radio" id="tab2" name="tab-group">
                    <label for="tab2" class="tab-title two"></label>
                    <section class="tab-content">
                        <div class="container">
                            <div class="owl-carousel owl-theme">
                                @foreach($catCatalogIndex as $goodsItem)
                                    <div class="tadItem hide">
                                        <a href="{{$root.$goodsItem['url']}}">
                                            <img src="@if($lang == 'en'){!! $goodsItem['tv_img_en'] !!}@else{!! $goodsItem['tv_img'] !!}@endif">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </section>
    @include("partials.rewievsBlock")
    @include("partials.videoRewievs")
    @include("partials.partnersBlock")
@endsection
