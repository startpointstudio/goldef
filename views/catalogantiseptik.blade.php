@extends('layouts.main')

@section('content')
    <div class="catalog-productov">
        {{$modx->runSnippet('leo#redirect')}}
        <section class="catalog slider-wrap ">
            <div class="">
                @foreach($sliderCatalog as $slide)
                    <div class="owlItem" style="background-image: url('{{$slide['img']}}')">
                        <div class="container">
                            <div class="section-tovar">
                                <h1><a href="[~{{$slide['id']}}~]">{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'pagetitle_'.$lang]) !!}</a></h1>
                            </div>
                            <div class="text-left hide">
                                <p>{{$slide['text']}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section class="dog-produkt-wrap" id="tov">
            <div class="container">
                <div class="row center-produkt">
                    @foreach($catalog as $item)
                        @if( $count>=7)
                            {!! $modx->getPlaceholder('count')  !!}
                            <div class="goodsItem-produkt six hide">
                                <a href="{{$root.$item['url']}}"><img src="@if($lang == 'en'){!! $item['tv_img_en'] !!}@else{!! $item['tv_img'] !!}@endif"></a>
                            </div>
                        @else
                            <div class="goodsItem-produkt hide">
                                <a href="{{$root.$item['url']}}"><img src="@if($lang == 'en'){!! $item['tv_img_en'] !!}@else{!! $item['tv_img'] !!}@endif"></a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>

        @include("partials.videoRewievs")
        @include("partials.rewievsBlock")
        @include("partials.partnersBlock")
    </div>
@endsection
