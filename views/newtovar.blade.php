@extends('layouts.main')

@section('content')
    <style>
        .slider-wrap h2{
            text-shadow: -1px 0 {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 0 1px {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 1px 0 {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 0 -1px {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}};
        }
    </style>
    <section class="slider-wrap">
        @if($documentObject['parent'] == 6)
            <div class="img-main-section" style="border-bottom:4px solid #f2b72e; background-size: cover; background-position-y: -120px; background-image: url('{!! $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'banner']) !!}')">
                @else
                    <div class="img-main-section" style="border-bottom:4px solid #4e4e4e; background-size: cover; background-image: url('{!! $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'banner']) !!}')">
                        @endif
                        <div class="container">
                            <h2>{!! $documentObject['pagetitle'] !!}</h2>
                            <p style="color:{{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-top'])}}; text-shadow: -1px 0 {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 0 2px {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 2px 0 {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}}, 0 -1px {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'color-head-bottom'])}};">{!! $documentObject['longtitle'] !!}</p>
                        </div>
                    </div>
    </section>
    <section class="golden-wrap-produkt">
        <div class="container">
            <div class="golden-produkt-top">
                <div class="golden-produkt-img"><img src="{!! $modx->runSnippet('DocInfo', ['docid' => $documentObject['id'], 'field' => 'img']) !!}" alt=""></div>
                <div class="golden-produkt-text">
                    <p>{!! $documentObject['description'] !!}</p>
                    <div class="box-text-golden">
                        <div class="advantages-text-golden" style="background-color: {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'bgcolor-content'])}};">
                            {!! $documentObject['content'] !!}
                        </div>
                        <div class="registr-certificate" style="background-color: {{$modx->runSnippet('DocInfo', ['docid' => $documentObject['parent'], 'field' => 'bgcolor-initiation'])}};">
                            <h3>Реєстраційне посвідчення</h3>
                            <div class="img">
                                @foreach($initiation as $img)
                                    <div class="img-one"><a href="{{$img['file']}}" download><img src="{{$img['img']}}" alt=""></a></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="golden-table new">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-end-text">
                            {!! $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'left-content']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-end-text">
                            {!! $modx->runSnippet('DocInfo', ['docid' => $modx->documentIdentifier, 'field' => 'right-content']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-produkt-wrap">
        <div class="text-produkt-all">Інші продукти ТМ {!! $documentObject['pagetitle'] !!}</div>
        <div class="container">
            <div class="row center-produkt-cat">
                <div class="owl-carousel owl-theme">
                    @foreach($otherCatalog as $dog)
                        <div class="col-sm-12">
                            <div class="goodsItem-produkt three">
                                <a href="{{$dog['url']}}"><img src="{{$dog['tv_img']}}"></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @include("partials.videoRewievs")
    <div class="tovar">
        @include("partials.partnersBlock")
    </div>
@endsection
