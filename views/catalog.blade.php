@extends('layouts.main')

@section('content')
    <div class="catalog-productov">
    {{$modx->runSnippet('leo#redirect')}}
    <section class="catalog slider-wrap ">
        <div class="">
            @foreach($sliderCatalog as $slide)
                <div class="owlItem" style="background-image: url('{{$slide['img']}}')">
                    <div class="container">
                        <div class="section-tovar">
{{--                            <img  class="hide" src="{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'img']) !!}" alt="" >--}}
                            <h1><a href="[~{{$slide['id']}}~]">{!! $modx->runSnippet('DocInfo', ['docid' => $slide['id'], 'field' => 'pagetitle']) !!}</a></h1>
                        </div>
                        <div class="text-left hide">
                            <p>{{$slide['text']}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section class="dog-produkt-wrap" id="tov">
        <div class="text-produkt-dog" style="background:linear-gradient(90deg, rgba(255,255,255,0) 0%, {{$modx->runSnippet('leo#colormenu')}} 25%, {{$modx->runSnippet('leo#colormenu')}} 50%, {{$modx->runSnippet('leo#colormenu')}} 75%, rgba(255,255,255,0) 100%)">{{ $modx->getConfig("__Protection_for_dogs") }}</div>
        <div class="container">
            <div class="row center-produkt">
                @foreach($dogCatalog as $dog)
                    @if( $count>=7)
                        {!! $modx->getPlaceholder('count')  !!}
                        <div class="goodsItem-produkt six hide">
                            <a href="{{$root.$dog['url']}}"><img src="@if($lang == 'en'){!! $dog['tv_img_en'] !!}@else{!! $dog['tv_img'] !!}@endif"></a>
                        </div>
                        @else
                        <div class="goodsItem-produkt hide">
                            <a href="{{$root.$dog['url']}}"><img src="@if($lang == 'en'){!! $dog['tv_img_en'] !!}@else{!! $dog['tv_img'] !!}@endif"></a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
        <section class="cat-produkt-wrap">
            @if($documentObject['id'] != 7)
                @if ($documentObject['id'] == 6)
                    <div class="text-produkt-cat" style="color:{{$modx->runSnippet('leo#colormenu')}};background:linear-gradient(90deg, rgba(255,255,255,0) 0%, {{$modx->runSnippet('leo#colortitle')}} 25%, {{$modx->runSnippet('leo#colortitle')}} 50%, {{$modx->runSnippet('leo#colortitle')}} 75%, rgba(255,255,255,0) 100%)">{{ $modx->getConfig("__Protection_for_cats") }}</div>
                    @else
                    <div class="text-produkt-cat" style="background:linear-gradient(90deg, rgba(255,255,255,0) 0%, {{$modx->runSnippet('leo#colormenu')}} 25%, {{$modx->runSnippet('leo#colormenu')}} 50%, {{$modx->runSnippet('leo#colormenu')}} 75%, rgba(255,255,255,0) 100%)">{{ $modx->getConfig("__Protection_for_cats") }}</div>
                @endif
            @endif
            <div class="container">
                <div class="row center-produkt-cat">
                    @foreach($catCatalog as $cat)
                        <div class="col-md-3  col-sm-6">
                            <div class="goodsItem-produkt hide">
                                <a href="{{$root.$cat['url']}}"><img src="@if($lang == 'en'){!! $cat['tv_img_en'] !!}@else{!! $cat['tv_img'] !!}@endif"></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @include("partials.videoRewievs")
    @include("partials.rewievsBlock")
    @include("partials.partnersBlock")
    </div>
@endsection