@extends('layouts.main')

@section('content')
    <section class="slider-wrap">
        <div class="img-main-section"><img src="{!! $modx->runSnippet('DocInfo', ['docid' => $documentObject['id'], 'field' => 'img', 'render' => '1']) !!}" alt=""></div>
    </section>
    <section class="contact">



        <div class="contact-section-box margin">

            <h1>{{ $documentObject['pagetitle_'.$lang] }}</h1>

            <div class="contact-text-box">



                <p>{{$modx->getConfig('client_field_addres_'.$lang)}}<br><a href="tel:{{$modx->getConfig('client_field_phone1')}}">{{$modx->getConfig('client_field_phone1')}}</a><br>
                <a href="tel:{{$modx->getConfig('client_field_phone2')}}">{{$modx->getConfig('client_field_phone2')}}</a>
                </p>
            </div>

            <div id="map-box-contact"></div>

            <h2>{{ $modx->getConfig("__Contact_us") }}</h2>
            <div class="contact-box-form">

                {!! $modx->runSnippet("FormLister", [
                'formTpl' => '@B_FILE:partials/ContactForm',
                'rules' => '{
                "name": {
                  "required": "'.$modx->getConfig("__error_name").'"
                },
                "contact": {
                  "required": "'.$modx->getConfig("__error_phone").'"
                },
                "text": {
                  "required": "'.$modx->getConfig("__error_text").'"
                }
                }',
                'from' => 'noreply@goldef.ua',
                'to' => $modx->getConfig("client_field_email_mail"),
                'formid'=>'basic',
                'protectSubmit' => '0',
                'submitLimit' => '0',
                'subject' => 'Сообщение с сайта',
                'successTpl' => '@B_FILE:partials/SuccessTpl',
                'messagesOuterTpl' => '@CODE: Ваше заявление успешно отправлено, в ближайшее время с Вами свяжуться',
                'reportTpl' => '@CODE:
                <p>Имя: [+name.value+]</p>
                <p>Контактные данные: [+contact.value+]</p>
                <p>Сообщение: [+text.value+]</p>',
            ]) !!}

            </div>
        </div>
    </section>
@endsection
